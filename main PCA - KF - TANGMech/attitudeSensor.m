function [attitudeMeasurement, sigma_psi] = attitudeSensor(constants, rDiff, C_bneighbor__bself, self_estimation, C_t__b_init, C_t__b1, seen)
    % Assumes sensor initially points directly forward

    % Naive approach for camera with some angular resolution
    
    % focal_length = 4.74 * 10^(-3);
    % Fnumber = 1.2;
    % wavelength = 550 * 10^(-9); % average visible light
    % 
    % % Using Raspberry Pi Cam 3, wide lens
    % 
    D = norm(rDiff);
    % %C_b1__b2 = C_t__b1.'*C_t__b2;
    [~,theta,psi] = math.dcm2rpy(C_t__b_init.'*C_t__b1);
    % 
    %self_estimation = math.q2dcm(self_estimation);
    % 
    % apertureDiameter = focal_length/Fnumber;
    % angularRes = 1.22 * wavelength / apertureDiameter; %1.69e-4

    angularRes = constants.angularRes; %degrees

    %sigma_psi = sqrt(angularRes^2/3);
    sigma_psi = angularRes;

    %angularRes = 0; %%% FIXME
   
    maxVheadingOffset = 270/2; %degrees
    maxHheadingOffset = 270/2; %degrees
    maxRange = 30;%meters
    
    %headingErr = headingErr * pi/180;
    
    Vheading = theta; %degrees
    Hheading = psi; %degrees


    if D<maxRange && seen
        
        % phi = 2*pi*rand(1,1);
        % costheta = rand(1,1);
        % theta = acos(costheta);
        % 
        % %angulardisturbance = angularRes*pi/180*randn(1);
        % angulardisturbance = angularRes*pi/180*randn(1); %%% FIXME
        % x = sin(theta)*cos(phi);
        % y = sin(theta)*sin(phi);
        % z = cos(theta);
        % 
        % k = angulardisturbance * [x; y; z]; %%%FIXME
        % 
        % dcmError = math.aa2dcm(k);
        % 
        % %FIXME
        % %dcmError = eye(3);
        % 
        % %C_t__bneighbor = self_estimation * dcmError * C_bneighbor__bself';
        % %q_t__bneighbor = math.q1xq2(math.q1xq2(self_estimation, math.dcm2q(dcmError)), math.dcm2q(C_bneighbor__bself'));

        q_t__bneighbor_ideal = math.q1xq2(self_estimation, math.dcm2q(C_bneighbor__bself'));

        RPY_t__bneighbor_ideal = math.q2rpy(q_t__bneighbor_ideal);

        rnoise = angularRes * pi/180 * randn(1);
        pnoise = angularRes * pi/180 * randn(1);
        ynoise = angularRes * pi/180 * randn(1);

        RPY_t__bneighbor_tilde = RPY_t__bneighbor_ideal + [rnoise; pnoise; ynoise];

        %q_t__bneighbor = math.rpy2q(RPY_t__bneighbor_tilde);
        q_t__bneighbor = q_t__bneighbor_ideal + [0; rnoise; pnoise; ynoise];
        q_t__bneighbor = math.normalize(q_t__bneighbor);

        %rangeMeasurement = self_estimation + dcmError*r_t__b1b2;
        %attitudeMeasurement = math.dcm2q(C_t__bneighbor);
        attitudeMeasurement = q_t__bneighbor;
        %attitudeMeasurement = q_t__bneighbor_ideal;
        %rangeMeasurement = self_estimation - rDiff;

    else
        attitudeMeasurement = "Damn, it's out of range :(";

    end

end