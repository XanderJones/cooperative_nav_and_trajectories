function swarm_cell_process(constants, swarm_cell, POSPLOT)
    % FUNCTION DESCRIPTION:
    %   Calls swarm plot on each trial to produce combo outputs
    %
    % INPUTS:
    %   swarm_cell: The output of mc_func
    %   POSPLOT: Whether to plot in 3D the positions (INTENSIVE)
    %
    % OUTPUTS:
    %   Outputs a plot showing relative and absolute position and orientation
    %   errors to the desired folderPath
    
    %close all
    for k = 1:numel(swarm_cell)/2
        for l = 1:numel(swarm_cell{1,k}.UAVList)
            current_swarm = swarm_cell{1,k}.UAVList{l};
            if constants.CLOSED_LOOP
                posarray{k,l} = current_swarm.INS.r_t__t_b_INS;
            else
                posarray{k,1} = current_swarm.INS.r_t__t_b_out;
            end
            gtarray{k,l} = swarm_cell{1,k}.UAVList{l}.INS.GT.r_t__t_b{l};
        end
    end
    
    
    for i = 1:numel(swarm_cell)/2
        swarm_cell{1,i} = swarmPlot(constants, swarm_cell{1,i}, [],1,1); 
    end

    %swarmPlot(constants, swarm_cell{1,i}, ["PVAOne"],1,1);
    
    mont = swarm_cell{2, 1};
    
    Np = size(swarm_cell{1,1}.PosError, 2);
    No = size(swarm_cell{1,1}.OriError, 2);
    pos_err_array = zeros(numel(swarm_cell)/2, Np);
    ori_err_array = zeros(numel(swarm_cell)/2, No);
    abs_pos_err_array = zeros(numel(swarm_cell)/2, Np);
    abs_ori_err_array = zeros(numel(swarm_cell)/2, No);
    
    for i = 1:numel(swarm_cell)/2
        pos_err_array(i,:) = swarm_cell{1, i}.PosError;
        ori_err_array(i,:) = swarm_cell{1, i}.OriError;
        abs_pos_err_array(i,:) = swarm_cell{1, i}.AbsPosError;
        abs_ori_err_array(i,:) = swarm_cell{1, i}.AbsOriError;
    end
    figure
    subplot(2, 3, 1)
    for i = 1:numel(swarm_cell)/2
        plot(1:Np-1, pos_err_array(i,1:end-1))
        hold on
    end
    hold off
    xlabel("Samples")
    ylabel("Position Error (m)")
    title("All RMSE Position Errors")
    grid on
    subplot(2, 3, 2)
    plot(1:No-1, ori_err_array(1:mont,1:end-1))
    xlabel("Samples")
    ylabel("Orientation Error (^\circ)")
    title("All Orientation Errors")
    grid on
    
    % MSE_pos_x = 1/numel(swarm_cell) .* sum(pos_err_array(1,:,:), 3).^2;
    % MSE_pos_y = 1/numel(swarm_cell) .* sum(pos_err_array(2,:,:), 3).^2;
    % MSE_pos_z = 1/numel(swarm_cell) .* sum(pos_err_array(3,:,:), 3).^2;
    % RMSE_pos_x = sqrt(MSE_pos_x);
    % RMSE_pos_y = sqrt(MSE_pos_y);
    % RMSE_pos_z = sqrt(MSE_pos_z);
    MSE_pos = 2/numel(swarm_cell)  .* sum(pos_err_array);
    RMSE_pos = sqrt(MSE_pos);
    % RMSE_pos_y = 2/numel(swarm_cell) .* sum(pos_err_array(2,:), 3);
    % RMSE_pos_z = 2/numel(swarm_cell) .* sum(pos_err_array(3,:), 3);
    % 
    MSE_ori = 2/numel(swarm_cell) .* sum(ori_err_array.^2);
    RMSE_ori = sqrt(MSE_ori);

    Abs_MSE_pos = 2/numel(swarm_cell) .* sum(abs_pos_err_array.^2);
    Abs_RMSE_pos = sqrt(Abs_MSE_pos);
    
    Abs_MSE_ori = 2/numel(swarm_cell) .* sum(abs_ori_err_array.^2);
    Abs_RMSE_ori = sqrt(Abs_MSE_ori);
    
    subplot(2, 3, 4)
    %plot(1:Np-1, RMSE_pos_x(1:end-1), 1:Np-1, RMSE_pos_y(1:end-1), 1:Np-1, RMSE_pos_z(1:end-1))
    plot(1:Np-1, RMSE_pos(1:end-1))
    %yline(.03,'k--','Lower Limit of Ranger Accuracy')
    yline(constants.neighbor_var,'k--','Ranger Accuracy')
    xlabel("Samples")
    ylabel("RMSE Pos. Rel. Error (m)")
    title("RMSE of Rel. Position Error")
    grid on
    
    subplot(2, 3, 5)
    plot(1:No-1, RMSE_ori(1:end-1))
    xlabel("Samples")
    ylabel("RMSE Ori. Rel. Error (^\circ)")
    title("RMSE of Rel. Orientation Error")
    grid on
    legend([sprintf("Angular Resolution of Sensor: %.3f degrees", constants.angularRes)])

    subplot(2, 3, 3)
    plot(1:Np-1, Abs_RMSE_pos(1:end-1))
    yline(3,'k--','GPS Accuracy')
    xlabel("Samples")
    ylabel("RMSE Pos. Abs. Error (m)")
    title("RMSE of Abs. Position Error")
    grid on
    
    subplot(2, 3, 6)
    plot(1:No-1, Abs_RMSE_ori(1:end-1))
    xlabel("Samples")
    ylabel("RMSE Ori. Abs. Error (^\circ)")
    title("RMSE of Abs. Orientation Error")
    grid on
    legend([sprintf("Angular Resolution of Sensor: %.3f degrees", constants.angularRes)])


    sgtitle(sprintf("Pos. & Ori. Error over %i Iterations", mont))






    if POSPLOT
        figure
        hold on
        grid on
        axis equal
        view(3)
        for j = 1:numel(swarm_cell)/2
            for m = 1:numel(swarm_cell{1,j}.UAVList)
                GTP = gtarray{j,m};
                plot3(GTP(1,1:end-1), GTP(2,1:end-1), GTP(3,1:end-1), 'r')
    
                P = posarray{j,m};
                %P = P.*[1.2;1.2;1];
                plot3(P(1,1:end-1), P(2,1:end-1), P(3,1:end-1), 'b')
            end
        end
        xlabel("East (m)")
        ylabel("North (m)")
        zlabel("Up (m)")
        legend(["Reference Trajectories", "Mechanized UAV Positions"])
        title("All UAVs position over time")
    end
end