function [velMeasurement, sigma_xyz] = velSensor(rDiff, self_estimation, C_t__b_init, C_t__b1)
    % Assumes sensor initially points directly forward

    % Values given for Hokuyo UTM-30LX-EW Scanning Laser Rangefinder (30m)
    D = norm(rDiff);
    %C_b1__b2 = C_t__b1.'*C_t__b2;
    [~,theta,psi] = math.dcm2rpy(C_t__b_init.'*C_t__b1);

    angularRes = 0.25; %degrees
    if D<10
        measAccuracy = 30*10^(-3); 
    else 
        measAccuracy = 50*10^(-3); %meters
    end

    sigma_xyz = sqrt(measAccuracy^2/3);

    
    maxVheadingOffset = 270/2; %degrees
    maxHheadingOffset = 270/2; %degrees
    maxRange = 30;%meters
    
    %headingErr = headingErr * pi/180;
    
    Vheading = theta; %degrees
    Hheading = psi; %degrees


    if D<maxRange && Vheading<maxVheadingOffset && Hheading<maxHheadingOffset
        
        phi = 2*pi*rand(1,1);
        costheta = rand(1,1);
        theta = acos(costheta);

        angulardisturbance = angularRes*pi/180*randn(1);

        x = sin(theta)*cos(phi);
        y = sin(theta)*sin(phi);
        z = cos(theta);

        k = angulardisturbance * [x; y; z];

        dcmError = math.aa2dcm(k);
        %C_b1__b2_tilde = dcmError*C_b1__b2;

        D = D + measAccuracy*randn(1);
        
        r_t__b2b1 = dcmError*((rDiff/norm(rDiff)) * D);
        r_t__b1b2 = -1*r_t__b2b1;

        %rangeMeasurement = self_estimation + dcmError*r_t__b1b2;
        rangeMeasurement = self_estimation - dcmError*r_t__b1b2;
        %rangeMeasurement = self_estimation - rDiff;

    else
        rangeMeasurement = "Damn, it's out of range :(";

    end

end