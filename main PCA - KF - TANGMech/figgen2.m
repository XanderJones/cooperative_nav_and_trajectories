primary_mont = 2;
% function mc_func(folderPath, mont, IMUnum, F_meas, trajectoryFile, ATT_AIDING, angRes, centerGPS, DEAD_RECKONING, ThreeD, GPSAll)

trajFiles = ["./trajFiles/gt_uav_f8.mat", "./trajFiles/Scurve.mat", "./trajFiles/gt_uav_7stationary.mat", "./trajFiles/IMUS_2.mat", "./trajFiles/IMUS_5.mat", "./trajFiles/IMUS_9.mat", "./trajFiles/IMUS_21.mat"];
IMUnames = ["KVH1750", "IMU.Orientus", "STIM300", "ADIS16500"];
GPSnames = ["NoGPSCenter", "GPSCenter"];
allGPSnames = ["NoAllGPS", "AllGPS"];
trajnames = ["Fig8", "SCurve", "Stationary", "2IMUs3Lobe", "5IMUs3Lobe", "9IMUs3Lobe", "21IMUs3Lobe"];
attnames = ["NoAttitudeAiding", "AttitudeAiding"];
noRELname = ["normal", "NO_RELATIVE_AIDING"];

attAid = 1;
noREL = 0;
IMUtype = 1;
GPSAll = 1;
GPS = 0;
trajFile = 1;
% 
% 
for UKF = 1
    for CLOSED_LOOP = 1
        GPSAll = 1;
        GPS = 1;

        name = strjoin([UKF,CLOSED_LOOP,IMUnames(IMUtype), "_", GPSnames(GPS+1), "_", allGPSnames(GPSAll+1), "_", trajnames(trajFile), "_", attnames(attAid+1), "_", noRELname(noREL+1)], "");
        mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, 1, 100, trajFiles(2), attAid, 0.25, 0, 0, 0, 1, noREL, UKF, CLOSED_LOOP)
        % 
        % GPSAll = 0;
        % GPS = 0;
        % 
        % name = strjoin([UKF,CLOSED_LOOP,IMUnames(IMUtype), "_", GPSnames(GPS+1), "_", allGPSnames(GPSAll+1), "_", trajnames(trajFile), "_", attnames(attAid+1), "_", noRELname(noREL+1)], "");
        % mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, 1, 100, trajFiles(2), attAid, 0.25, 0, 0, 0, 0, noREL, UKF, CLOSED_LOOP)
    end
end
% for IMUtype = 1
%     for trajFile = 2
%         for GPS = 0:1
%             for GPSAll = 1
% 
%                     if ~(GPSAll && GPS)
%                         name = strjoin([IMUnames(IMUtype), "_", GPSnames(GPS+1), "_", allGPSnames(GPSAll+1), "_", trajnames(trajFile), "_", attnames(attAid+1), "_", noRELname(noREL+1)], "");
%                         mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, IMUtype, 10, trajFiles(trajFile), attAid, 0.25, GPS, 0, 0, GPSAll, noREL)
%                     end
% 
%             end
%         end
%     end
% end
% 
% noREL = 1;
% name = strjoin([IMUnames(IMUtype), "_", GPSnames(GPS+1), "_", allGPSnames(GPSAll+1), "_", trajnames(trajFile), "_", attnames(attAid+1), "_", noRELname(noREL+1)], "");
% mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, IMUtype, 10, trajFiles(trajFile), attAid, 0.25, GPS, 0, 0, GPSAll, noREL)