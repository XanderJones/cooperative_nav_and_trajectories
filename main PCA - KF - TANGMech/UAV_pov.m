function UAV_pov(swarm)

    UAV_List = swarm.UAVList;
    numIMUs = numel(UAV_List);

    fov_angle = 270;
    D = 10;

    pos_list = zeros(3, numIMUs);
    for i = 1:numIMUs
        pos_list(:, i) = UAV_List{i}.INS.r_t__t_b_INS(:,1);
        ori_list(:, :, i) = UAV_List{i}.INS.C_t__b_INS(:,:,1);
    end

    % Plot each camera's field of view
    figure;
    %subplot(2, 1, 1)
    hold on;
    
    for i = 1:numIMUs
        % Define cone parameters
        apex = pos_list(:, i);
        orientation_matrix = ori_list(:, :, i) * ori_list(:,:,1)';

        scatter3(apex(1), apex(2), apex(3), 'filled', 'r')
        ests = swarm.est_counter(i) - 1;
        text(apex(1), apex(2), apex(3), sprintf('UAV #%i: seen by: %i UAVs', i, ests))
        
        % % Generate cone points
        % cone_points = generateConePoints(apex, fov_angle);
        % 
        % % Transform cone points based on camera orientation
        % %transformed_points = transformPoints(orientation_matrix, [cone_points_x(:), cone_points_y(:), cone_points_z(:)]);
        % transformed_points = orientation_matrix * cone_points + apex;
        % 
        % % Plot the cone
        % fill3(transformed_points(:, 1), transformed_points(:, 2), transformed_points(:, 3), 'b', 'FaceAlpha', 0.5);
        
        [X,Y,Z] = sphere(1000);
        X = X * D;
        Y = Y * D;
        Z = Z * D;

        xrot = [0, 0, 0, 0, 0, 0, 0];
        yrot = [0, 0, 0, 0, 0, 0, 0];
        zrot = [0, 0, 0, 0, 0, 0, 0];

        M=makehgtform('translate',apex,'xrotate',xrot(i),'yrotate',yrot(i),'zrotate',zrot(i));
        % 
        % 
        radicand = D^2 * (1 + (sin(fov_angle*pi/360))^2);
        a = sqrt(radicand);

        [Xc,Yc,Zc]=cylinder([0 1],1000);
        % X = X(2,:);
        % Y = Y(2,:);
        % Z = Z(2,:);
        Xc = Xc * -2/3*a;
        Yc = Yc * -2/3*a;
        Zc = Zc * -a/2;

        Z(Z<-a/2) = NaN;

        %Zc(2,:) = -a/2;

        % mx = X>Xc(2,:);
        % my = Y>Yc(2,:);
        % mz = Z>Zc(2,:);
        % X(my & mz) = NaN;
        % Y(mx & mz) = NaN;
        % Z(mx & my) = NaN;

        % cylinder_in_sphere = ((Xc(2,:)-apex(1)).^2 + (Yc(2,:)-apex(2)).^2 + (Zc(2,:)-apex(3)).^2) < D^2;
        % Xc(2,cylinder_in_sphere) = NaN;
        % Yc(2,cylinder_in_sphere) = NaN;
        % Zc(2,cylinder_in_sphere) = NaN;
        

        h=surf(X,Y,Z,'FaceColor','blue','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.05);
        h=surf(Xc,Yc,Zc,'FaceColor','blue','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.05);


        xlabel("X-axis (East)")
        ylabel("Y-axis (North)")
        zlabel("Z-axis (Down)")
       % h = surf(X>Xc, Y>Yc, Z>Zc)
        % m1 = X.^2 + Y.^2 + Z.^2 > (D)^2;
        % X(m1) = NaN;
        % Y(m1) = NaN;
        % Z(m1) = NaN;
        % %axis([-3 +3,-3 +3,-3 +3])
        % [r, p, y] = math.dcm2rpy(orientation_matrix);
        % M=makehgtform('translate',apex,'xrotate',-r,'yrotate',-p,'zrotate',-y);
        % h=surf(X,Y,Z,'FaceColor','red','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.1);
        % %view([-75,35])
        axis tight
        view(90.4505, 28.8296)

        grid on
    end
    figure
    %subplot(2, 1, 2)
        hold on
        for i = 1:numIMUs
            apex = pos_list(:, i);
            if i <= 4
                scatter3(apex(1), apex(2), apex(3), 'filled', 'b')
            else
                scatter3(apex(1), apex(2), apex(3), 'filled', 'r')
            end
        end
        for i = numIMUs
        % Define cone parameters
        apex = pos_list(:, i);
        orientation_matrix = ori_list(:, :, i);

        scatter3(apex(1), apex(2), apex(3), 'filled', 'r')
        ests = swarm.est_counter(i) - 1;
        text(apex(1), apex(2), apex(3), sprintf('UAV #%i: sees: 4 UAVs', i))
        
        % % Generate cone points
        % cone_points = generateConePoints(apex, fov_angle);
        % 
        % % Transform cone points based on camera orientation
        % %transformed_points = transformPoints(orientation_matrix, [cone_points_x(:), cone_points_y(:), cone_points_z(:)]);
        % transformed_points = orientation_matrix * cone_points + apex;
        % 
        % % Plot the cone
        % fill3(transformed_points(:, 1), transformed_points(:, 2), transformed_points(:, 3), 'b', 'FaceAlpha', 0.5);
        
        [X,Y,Z] = sphere(1000);
        X = X * D;
        Y = Y * D;
        Z = Z * D;
        xrot = [0, 0, 0, 0, 0, 0, 0];
        yrot = [0, 0, 0, 0, 0, 0, 0];
        zrot = [0, 0, 0, 0, 0, 0, 0];

        M=makehgtform('translate',apex,'xrotate',xrot(i),'yrotate',yrot(i),'zrotate',zrot(i));
        % 
        % 
        radicand = D^2 * (1 + (sin(fov_angle*pi/360))^2);
        a = sqrt(radicand);

        [Xc,Yc,Zc]=cylinder([0 1],1000);
        % X = X(2,:);
        % Y = Y(2,:);
        % Z = Z(2,:);
        Xc = Xc * -2/3*a;
        Yc = Yc * -2/3*a;
        Zc = Zc * -a/2;

        Z(Z<-a/2) = NaN;

        %Zc(2,:) = -a/2;

        % mx = X>Xc(2,:);
        % my = Y>Yc(2,:);
        % mz = Z>Zc(2,:);
        % X(my & mz) = NaN;
        % Y(mx & mz) = NaN;
        % Z(mx & my) = NaN;

        % cylinder_in_sphere = ((Xc(2,:)-apex(1)).^2 + (Yc(2,:)-apex(2)).^2 + (Zc(2,:)-apex(3)).^2) < D^2;
        % Xc(2,cylinder_in_sphere) = NaN;
        % Yc(2,cylinder_in_sphere) = NaN;
        % Zc(2,cylinder_in_sphere) = NaN;
        

        h=surf(X,Y,Z,'FaceColor','blue','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.05);
        h=surf(Xc,Yc,Zc,'FaceColor','blue','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.05);


        xlabel("X-axis (East)")
        ylabel("Y-axis (North)")
        zlabel("Z-axis (Down)")
       % h = surf(X>Xc, Y>Yc, Z>Zc)
        % m1 = X.^2 + Y.^2 + Z.^2 > (D)^2;
        % X(m1) = NaN;
        % Y(m1) = NaN;
        % Z(m1) = NaN;
        % %axis([-3 +3,-3 +3,-3 +3])
        % [r, p, y] = math.dcm2rpy(orientation_matrix);
        % M=makehgtform('translate',apex,'xrotate',-r,'yrotate',-p,'zrotate',-y);
        % h=surf(X,Y,Z,'FaceColor','red','Parent',hgtransform('Matrix',M),'LineStyle','none','FaceAlpha',0.1);
        % %view([-75,35])
        axis tight
        view(65.4339, 10.7509)
        grid on

    % function [cone_points_loc] = generateConePoints(apex_loc, fov_angle)
    %     % Generate cone points based on apex and field of view angle
    %     num_points = 100;  % Adjust as needed
    % 
    %     theta = linspace(0, 2*pi, num_points);
    %     radius = tan(fov_angle * pi/360);
    % 
    %     x = apex_loc(1) + radius * cos(theta);
    %     y = apex_loc(2) + radius * sin(theta);
    %     z = apex_loc(3) * ones(size(theta));
    % 
    %     cone_points_loc = [x; y; z];
    % end
    % 
    % function transformed_points = transformPoints(orientation_matrix, points)
    %     % Transform points based on orientation matrix
    %     rotated_points = points * orientation_matrix.';
    %     transformed_points = rotated_points + apex;
    % end

end