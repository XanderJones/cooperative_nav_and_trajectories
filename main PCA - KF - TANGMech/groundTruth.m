classdef groundTruth < handle
    % OBJECT DESCRIPTION:
    %   defines the ground truth data from the trajectory file
    %
    % PROPERTIES:
    %   gt is the way ground truth data is accessed. It a struct object
    %   N is the number of UAVs
    %
    % METHODS:
    %   The object constructor creates all necessary values in the gt
    %   property so no methods outside of it are used

    properties (SetAccess = private)
        gt
    end

    properties (SetAccess = public)
        N
    end

    methods

        function obj = groundTruth(constants, trajFile)
            file = trajFile;
            load(file) %#ok<LOAD>

            N = size(gt.C_t__b,2); %#ok<NODEF>
            t = gt.constants.t_start:1/gt.constants.Fs:gt.constants.t_end;    % Discrete time vector [s]
            K = length(t);
            gt.K = K;
            gt.numIMUs = N;
            delT = diff(t);

            %% Populate more ground truth data from local tangential frame data
            gt.C_e__b = cell(1,N);  % Cell vector with rotation from agent n body frame to ECEF frame
            gt.r_e__eb = cell(1,N); % Cell vector with position of agent n body wrt ECEF frame, resolved in ECEF frame
            gt.v_e__eb = cell(1,N); % Cell vector with velocity of agent n body wrt ECEF frame, resolved in ECEF frame
            gt.a_e__eb = cell(1,N); % Cell vector with acceleration of agent n body wrt ECEF frame, resolved in ECEF frame
            
            % Rotation from local tangential frame to ECEF frame
            sL = sin(gt.constants.L_0 * pi/180);
            cL = cos(gt.constants.L_0 * pi/180);
            sl = sin(gt.constants.lambda_0 * pi/180);
            cl = cos(gt.constants.lambda_0 * pi/180);

            gt.C_e__t = [-sL*cl   -sl   -cL*cl;...
                         -sL*sl   cl    -cL*sl;...
                         cL       0     -sL];
            gt.r_e__et = nav.llh2xyz(constants, gt.constants.L_0 * pi/180, gt.constants.lambda_0 * pi/180, gt.constants.h_0);



                   
            % ECEF frame
            for n = 1:N
                gt.C_e__b{n} = zeros(3,3,K);
                gt.r_e__eb{n} = zeros(3,K);
                gt.v_e__eb{n} = zeros(3,K);
                gt.a_e__eb{n} = zeros(3,K);

                gt.v_t__t_b{n} = (gt.r_t__t_b{n}(:,2:end) - gt.r_t__t_b{n}(:,1:end-1))/constants.dt;
                gt.v_t__t_b{n} = [gt.v_t__t_b{n}, gt.v_t__t_b{n}(:,end)];
    
                gt.a_t__t_b{n} = (gt.v_t__t_b{n}(:,2:end) - gt.v_t__t_b{n}(:,1:end-1))/constants.dt;  % Acceleration (numerically) of n-frame wrt e-frame in the e-frame
                gt.a_t__t_b{n} = [gt.a_t__t_b{n}, gt.a_t__t_b{n}(:,end)];
                
                for k = 1:K
                    gt.C_e__b{n}(:,:,k) = gt.C_e__t*gt.C_t__b{n}(:,:,k);
                    gt.r_e__eb{n}(:,k) = gt.r_e__et + gt.C_e__t*gt.r_t__t_b{n}(:,k);
                    
                    % This was calculated, not a naive assumption!
                    gt.v_e__eb{n}(:,k) = gt.C_e__t*gt.v_t__t_b{n}(:,k);
                    gt.a_e__eb{n}(:,k) = gt.C_e__t*gt.a_t__t_b{n}(:,k);
                end
            end
            
            %% Synthesize clean sensor data
            gt.w_b__ib = cell(N,1);  % Cell vector with inertial rotation velocity of the agents
            gt.f_b__ib = cell(N,1);  % Cell vector with inertial specific force of the agents
            
            for n = 1:N
                w_b__ib = zeros(3,K-1); % w_b__ib for agent n
                f_b__ib = zeros(3,K-1); % f_b__ib for agent n
                
                for k = 1:K-1
                    % Synthesize gyro measurement, w_b__ib
                    % C_e__b_k = gt.C_e__b{n}(:,:,k);
                    % C_b__e_k = C_e__b_k.';
                    % Omega_e__eb = ((gt.C_e__b{n}(:,:,k+1)-C_e__b_k)./delT(k))*C_b__e_k;  % Groves Eq. 2.28
                    % w_e__eb = math.ss2vec(Omega_e__eb);
                    % constants.w_e__ie = [0;0;gt.constants.w_ie];
                    % constants.Omega_e__ie = math.vec2ss(constants.w_e__ie);
                    % w_b__ib(:,k) = C_b__e_k*(constants.w_e__ie + w_e__eb);  % Groves Eq. 2.25
                    % 

                    constants.w_e__ie = [0;0;gt.constants.w_ie];
                    constants.Omega_e__ie = math.vec2ss(constants.w_e__ie);
                    
                    if constants.QUATERNIONangularVel
                        C_t__b_k = gt.C_t__b{n}(:,:,k);
                        if k ~= 1 && norm(C_t__b_k - eye(3)) ~= 0 % When no rotation happens, only account for rotation of earth, the norm thing is checking for identity
                            if k == 4000
                                stophere = 1;
                            end
                            C_t__b_km1 = gt.C_t__b{n}(:,:,k-1);
                            C_bkm1__bk = C_t__b_km1'*C_t__b_k;
                            
                            q_bkm1__bk = math.dcm2q(C_bkm1__bk);
                            ksin = q_bkm1__bk(2:4, 1);
    
                            kvec = ksin ./ norm(ksin);
                            cosdtheta2 = q_bkm1__bk(1, 1);
    
                            dtheta = acos(cosdtheta2) * 2;
        
                            dt = constants.dt;
        
                            w_b__ib(:,k) = kvec*dtheta/dt + C_t__b_k.'*gt.C_e__t.'*constants.w_e__ie;
                        else
                            w_b__ib(:,k) = [0;0;0] + C_t__b_k.'*gt.C_e__t.'*constants.w_e__ie;
                        end

                    else
                        C_e__b_k = gt.C_e__b{n}(:,:,k);
                        C_b__e_k = C_e__b_k.';
                        Omega_e__eb = ((gt.C_e__b{n}(:,:,k+1)-C_e__b_k)./delT(k))*C_b__e_k;  % Groves Eq. 2.28
                        w_e__eb = math.ss2vec(Omega_e__eb);
                        
                        w_b__ib(:,k) = C_b__e_k*(constants.w_e__ie + w_e__eb);  % Groves Eq. 2.25

                    end


                    % Synthesize accelerometer measurement, f_b__ib
                    a_e__ib_k = gt.a_e__eb{n}(:,k) + 2*constants.Omega_e__ie*gt.v_e__eb{n}(:,k) + constants.Omega_e__ie*constants.Omega_e__ie*gt.r_e__eb{n}(:,k);   % Groves Eq. 2.97
                    f_b__ib(:,k) = gt.C_e__b{n}(:,:,k).'*(a_e__ib_k - nav.gamma__i_b(constants, gt.r_e__eb{n}(:,k))); % Groves Eq. 2.76
                end
                
                % Save measurements to cell vector
                gt.w_b__ib{n} = w_b__ib;
                gt.f_b__ib{n} = f_b__ib;

                
            end

            %% Synthesize measured data
            % gt.w_b__ib_tilde = cell(N,1);  % Cell vector with inertial rotation velocity of the agents
            % gt.f_b__ib_tilde = cell(N,1);  % Cell vector with inertial specific force of the agents
            % 
            % for n = 1:N
            %     constants_copy = constants; % Reset bias and random walk per each agent
            %     gt.w_b__ib_tilde{n} = zeros(3,K-1);
            %     gt.f_b__ib_tilde{n} = zeros(3,K-1);
            % 
            %     for k = 1:K-1
            %         [gt.w_b__ib_tilde{n}(:,k), gt.f_b__ib_tilde{k}(:,k), constants] = nav.IMU(constants_copy,gt.w_b__ib{n}(:,k),gt.f_b__ib{n}(:,k));
            %     end
            % end
            % 
            % if constants.IDEALIMU
            %     gt.w_b__ib_tilde = gt.w_b__ib;
            %     gt.f_b__ib_tilde = gt.f_b__ib;
            % end


            obj.gt = gt;

        end

    end
end