function rpy = q2rpy(q)
    w = q(1);
    x = q(2);
    y = q(3);
    z = q(4);

    % r = atan2(2*(w*x+y*z), 1-2*(x^2+y^2));
    % p = -pi/2 + 2*atan2(sqrt(1+2*(w*y-x*z)), sqrt(1-2*(w*y-x*z)));
    % y = atan2(2*(w*z+x*y), 1-2*(y^2+z^2));

    % roll  = atan2(2.0 * (z * y + w * x) , 1.0 - 2.0 * (x * x + y * y));
    % pitch = asin(2.0 * (y * w - z * x));
    % yaw   = atan2(2.0 * (z * w + x * y) , - 1.0 + 2.0 * (w * w + x * x));
    % 
    % rpy = [roll; pitch; yaw];

    t0 = 2.0 * (w * x + y * z);
    t1 = 1.0 - 2.0 * (x * x + y * y);
    roll = atan2(t0, t1);
    t2 = 2.0 * (w * y - z * x);

    
    if t2 > 1.0
        t2 = 1.0;
    end

    if t2 < -1.0 
        t2 = -1.0;
    end

    pitch = asin(t2);
    t3 = 2.0 * (w * z + x * y);
    t4 = 1.0 - 2.0 * (y * y + z * z);
    yaw = atan2(t3, t4);
    rpy = [roll, pitch, yaw];
end