function C_out = update_att(x, C_in)

    q_in = math.dcm2q(C_in);
    
    dq = math.k2q(x(1:3));
    
    q_out = math.q1xq2(dq, q_in);
    q_out_norm = math.normalize(q_out);
    
    C_out = math.q2dcm(q_out_norm);

end
