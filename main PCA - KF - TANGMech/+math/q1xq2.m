function [q] = q1xq2(q1,q2)
    qs=q1(1);
    qx=q1(2);
    qy=q1(3);
    qz=q1(4);

    qvec=[qx;qy;qz];

    ps=q2(1);
    px=q2(2);
    py=q2(3);
    pz=q2(4);

    pvec=[px;py;pz];

    qfs=qs*ps-dot(qvec,pvec);
    qfvec=qs*pvec+ps*qvec+cross(qvec,pvec);

    q=[qfs;
        qfvec];

end
