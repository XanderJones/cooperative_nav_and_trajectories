function [C] = q2dcm(q)
    % This just uses the formula outlined in the lecture
    qs = q(1);
    qx = q(2);
    qy = q(3);
    qz = q(4);

    r11 = qs^2+qx^2-qy^2-qz^2;
    r12 = 2*(qx*qy-qs*qz);
    r13 = 2*(qx*qz+qs*qy);
    r21 = 2*(qx*qy+qs*qz);
    r22 = qs^2-qx^2+qy^2-qz^2;
    r23 = 2*(qy*qz-qs*qx);
    r31 = 2*(qx*qz-qs*qy);
    r32 = 2*(qy*qz+qs*qx);
    r33 = qs^2-qx^2-qy^2+qz^2;

    C = [r11, r12, r13; r21, r22, r23; r31, r32, r33];
end
