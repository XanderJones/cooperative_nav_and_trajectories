function q = normalize(qin)
    q = qin ./ norm(qin);
end