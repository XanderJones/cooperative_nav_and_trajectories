function [k, km] = q2k(q)
   if q(1) > 1
       q = math.normalise(q);
   end
   theta = 2 * acos(q(1));
   s = sqrt(1 - q(1)^2);
   if (s < 1e-3)
     x = q(2);
     y = q(3);
     z = q(4);
   else
     x = q(2) / s;
     y = q(3) / s;
     z = q(4) / s;
   end
   k = theta * [x; y; z];

   theta = 2*pi - 2 * acos(q(1));
   s = sqrt(1 - q(1)^2);
   if (s < 1e-3)
     x = q(2);
     y = q(3);
     z = q(4);
   else
     x = q(2) / s;
     y = q(3) / s;
     z = q(4) / s;
   end
   km = theta * [x; y; z];
   

end