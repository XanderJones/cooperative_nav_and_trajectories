function [phi,theta,psi]=dcm2rpy(C)
    r11=C(1,1);
    r21=C(2,1);
    r31=C(3,1);
    r32=C(3,2);
    r33=C(3,3);
    
    psi=atan2d(r21,r11);
    phi=atan2d(r32,r33);
    
    denm=r32*sind(phi)+r33*cosd(phi);
    theta=atan2d(-r31,denm);


    if r11==0 || r33==0 ||denm==0
        fprintf('ATTENTION! ATAN function given undefined value. Answer may be incorrect!')
    end
end