function q = rpy2q(rpy)
    
    roll = rpy(1);
    pitch = rpy(2);
    yaw = rpy(3);
    % 
    % w = cos(r/2)*cos(p/2)*cos(y/2)+sin(r/2)*sin(p/2)*sin(y/2);
    % x = sin(r/2)*cos(p/2)*cos(y/2)-cos(r/2)*sin(p/2)*sin(y/2); 
    % y = cos(r/2)*sin(p/2)*cos(y/2)+sin(r/2)*cos(p/2)*sin(y/2); 
    % z = cos(r/2)*cos(p/2)*sin(y/2)-sin(r/2)*sin(p/2)*cos(y/2);
    % 
    % q = [w;x;y;z];

    % roll_q = [cos(r/2);sin(r/2);0;0];
    % pitch_q = [cos(p/2);0;sin(p/2);0];
    % yaw_q = [cos(y/2);0;0;sin(y/2)];
    % 
    % q = math.q1xq2(math.q1xq2(roll_q,pitch_q), yaw_q);

    qx = sin(roll/2) * cos(pitch/2) * cos(yaw/2) - cos(roll/2) * sin(pitch/2) * sin(yaw/2);
    qy = cos(roll/2) * sin(pitch/2) * cos(yaw/2) + sin(roll/2) * cos(pitch/2) * sin(yaw/2);
    qz = cos(roll/2) * cos(pitch/2) * sin(yaw/2) - sin(roll/2) * sin(pitch/2) * cos(yaw/2);
    qw = cos(roll/2) * cos(pitch/2) * cos(yaw/2) + sin(roll/2) * sin(pitch/2) * sin(yaw/2);
    q = -[qw; qx; qy; qz;];

end