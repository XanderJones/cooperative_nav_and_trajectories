function [q] = dcm2q(C)
    % Using the provided formula, and using the non normalized version
    k = math.dcm2k(C);
    theta = norm(k);
    K = k ./ theta;
    q__s = cos(theta/2);
    q_vec = K .* sin(theta/2);
    q = [q__s; q_vec];

    % Better version from VectorNav (hopefully avoids singularities)
    % VectorNav quaternion notation puts q4 as the scalar term. WE DONT,
    % so we arrange the terms slightly different.

    q_1s = .25 * (1 + C(1,1) - C(2,2) - C(3,3));
    q_2s = .25 * (1 - C(1,1) + C(2,2) - C(3,3));
    q_3s = .25 * (1 - C(1,1) - C(2,2) + C(3,3));
    q_4s = .25 * (1 + C(1,1) + C(2,2) + C(3,3));

    q_s_temp = [q_1s, q_2s, q_3s, q_4s];

    [~, ind] = max(q_s_temp);
    
    switch ind
        case 1
            q_1 = sqrt(q_1s);
            x = 4 * q_1s;
            y = C(1,2) + C(2,1);
            z = C(3,1) + C(1,3);
            w = C(2,3) - C(3,2);
            q = (1 / (4*q_1)) .* [w;x;y;z];
        case 2
            q_2 = sqrt(q_2s);
            x = C(1,2) + C(2,1);
            y = 4 * q_2s;
            z = C(2,3) + C(3,2);
            w = C(3,1) - C(1,3);
            q = (1 / (4*q_2)) .* [w;x;y;z];
        case 3
            q_3 = sqrt(q_3s);
            x = C(3,1) + C(1,3);
            y = C(2,3) + C(3,2);
            z = 4 * q_3s;
            w = C(1,2) - C(2,1);
            q = (1 / (4*q_3)) .* [w;x;y;z];
        case 4
            q_4 = sqrt(q_4s);
            x = C(2,3) - C(3,2);
            y = C(3,1) - C(1,3);
            z = C(1,2) - C(2,1);
            w = 4 * q_4s;
            q = (1 / (4*q_4)) .* [w;x;y;z];
    end
    q = math.normalize(q) .* [1;-1;-1;-1];

end
