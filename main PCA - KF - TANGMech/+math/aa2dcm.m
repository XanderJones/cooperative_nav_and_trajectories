function R = aa2dcm(r)
    theta = norm(r);

    Vt = 1-cos(theta);
    Ct = cos(theta);
    St = sin(theta);

    r11 = r(1)^2*Vt + Ct;
    r12 = r(1)*r(2)*Vt - r(3)*St;
    r13 = r(1)*r(3)*Vt + r(2)*St;
    r21 = r(1)*r(2)*Vt + r(3)*St;
    r22 = r(2)^2*Vt + Ct;
    r23 = r(2)*r(3)*Vt - r(1)*St;
    r31 = r(1)*r(3)*Vt - r(2)*St;
    r32 = r(2)*r(3)*Vt + r(1)*St;
    r33 = r(3)^2*Vt + Ct;

    R = [r11, r12, r13;
         r21, r22, r23;
         r31, r32, r33];

end
