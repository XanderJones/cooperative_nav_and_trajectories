%close all
clear 

mont = 3;

swarm_cell = cell(2, mont);
swarm_cell{2, 1} = mont;

%%% Write to a file
tic
for m = 1:mont
    
    clear swarm_current
    IMU.KVH1750
    % IMU.Orientus
    % IMU.STIM300
    % IMU.ADIS16500

    %trajectoryFile = './trajFiles/gt_uav_7stationary.mat';
    %trajectoryFile = './trajFiles/gt_f8_100Hz.mat';
    %trajectoryFile = './trajFiles/Scurve.mat';
    trajectoryFile = './trajFiles/gt_uav_f8.mat';
    
    %trajectoryFile = './trajFiles/IMUS_21.mat';

    load(trajectoryFile)
    [~, numIMUs] = size(gt.C_t__b);
    [~, b] = size(gt.r_t__t_b{1});
    N = b - 1;

    PCACounter = 0;
     
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % NUM AGENTS REDUCED BY 1 %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    load_constants;

    constants.allGPS = 0;
    
    constants.INSType = "PCATEST";
    constants.reduceIMUnum = 1;
    constants.F_measure = 100;
    constants.distanceThreshold = 10;
    %constants.neighbor_var = 30e-3;
    constants.neighbor_var = 5e-2;
    
    constants.ATT_AIDING = 1;
    constants.angularRes = .25;
    
    %numIMUs = 21;
    constants.weightingVec = ones(1,numIMUs);
    
    constants.IMU_CALIBRATED = 1;

    constants.centerGPS = 0;
    
    %N = 6000;
   

    %numIMUs = 21;
    swarm_current = swarmFactory(imu_const, trajectoryFile, constants, numIMUs);
    disp("ITERATING")
    lastpercent = 0;
    for i = 1:N-1
        swarm_current = swarm_current.updatePCA(i);  
        swarm_current = swarm_current.iterate(i); 
        if mod(floor(i/N * 100), 1) == 0 && lastpercent ~= floor(i/N*100)
            disp(['TEST: ', num2str(m), ' of ', num2str(mont), ' is ', num2str(floor(i / N * 100)), '% complete'])   
            lastpercent = floor(i/N*100);
        end
    end

    CumulativeTestTime = toc;
    AvgTestTime = CumulativeTestTime / m;
    remainingTime = seconds((mont - m) * AvgTestTime);

    remainingTime.Format = 'hh:mm:ss';

    disp(['TEST: ', num2str(m), ' is DONE - remaining time: ', char(remainingTime)])
    swarm_cell{1, m} = swarm_current;
end
swarm_cell_process(constants, swarm_cell, 1)
%save('monte_carlo.mat', 'swarm_cell')