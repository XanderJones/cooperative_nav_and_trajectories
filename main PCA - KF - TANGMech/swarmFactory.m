function swarmTemp = swarmFactory(imu_const, trajFile, constants, numIMUs)
    % FUNCTION DESCRIPTION:
    %   This outputs a swarm object from the trajectory file given
    %
    % INPUTS:
    %   imu_const: define the IMU
    %   trajFile: Give the folder path for the desired trajectory
    %   constants: Give the constants struct object
    %   numIMUs: redundant - match to the trajFile
    %
    % OUTPUTS:
    %   Outputs a swarm object


    disp("IMPORTING GROUND TRUTH")
    groundTruth1 = groundTruth(constants, trajFile);
    %constants.numIMUs = groundTruth1.gt.numIMUs;

    constants.C_e__t = groundTruth1.gt.C_e__t;
    constants.r_e__e_t = groundTruth1.gt.r_e__et;
    constants.imu_const = imu_const;

    constants.numIMUs = numIMUs;

    if constants.reduceIMUnum > 0
        numIMUs = numIMUs - constants.reduceIMUnum;
        constants.numIMUs = numIMUs;
    end
    
    
    GC_PCA_handle = GC_PCA(numIMUs, groundTruth1.gt);
    swarmTemp = Swarm(constants, GC_PCA_handle);

    for i = 1:numIMUs
        IMU1 = IMU(i, groundTruth1.gt,constants);
        if constants.INSType == "PCATEST"
            INS = PCATEST(i, constants, GC_PCA_handle, groundTruth1.gt, imu_const, groundTruth1.gt.K);
        end
        if constants.INSType == "GPS_Aided"
            INS = GPS_Aided(i, constants, GC_PCA_handle, groundTruth1.gt, imu_const, groundTruth1.gt.K);
        end
        if constants.INSType == "GPS_Consensus_Aided"
            INS = GPS_Consensus_Aided(i, constants, GC_PCA_handle, groundTruth1.gt, imu_const, groundTruth1.gt.K);
        end
        swarmTemp.UAVList{i} = UAV(i, IMU1, INS);
    end
    
    

end