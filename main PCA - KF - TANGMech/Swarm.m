classdef Swarm < handle
    % OBJECT DESCRIPTION:
    %   The Swarm class should hold UAV objects and is the primary way to
    %   define swarm behavior such as iterating UAVs and defining
    %   leader/follower interactions
    %
    % PROPERTIES:
    %   Not all properties are currently being used, but primarily, the
    %   UAVList property stores the UAV objects in a cell array. The MSE
    %   and RMSE properties store values for use in data processing later.
    %
    % METHODS:
    %   iterate: allows every UAV to be iterated by a top-level method call
    %   updatePCA: the name is outdated. No PCA is being performed. This is
    %   where GFC is performed for both position and attitude.
    
    properties
        UAVList
        GC_PCA %potentially unused
        PCA_Update
        constants

        est_counter

        prevQ

        numIMUs

        C_e__s_INIT
        counter

        C_s2__s1
        
        testprop

        MSE
        RMSE
        PosError
        OriError

        AbsMSE
        AbsRMSE
        AbsPosError
        AbsOriError
    end
    
    methods
        function obj = Swarm(constants, GC_PCA)
            obj.counter = 0;
            obj.constants = constants;
            obj.numIMUs = constants.numIMUs;
            obj.GC_PCA = GC_PCA;
            obj.prevQ = zeros(4, obj.numIMUs);
            obj.testprop = zeros(1, 30000);
        end
        
        function obj = iterate(obj, k)
            for i = 1:numel(obj.UAVList)
                obj.UAVList{i} = obj.UAVList{i}.iterate(k);
            end
        end

        function [obj] = updatePCA(obj, i)
            %w = obj.constants.weightingVec;
            
            if ~obj.constants.NOCONSENSUS
                % General Form Consensus
                N = obj.numIMUs;
                w = ones(1, N);%%%FIXME
                estimationCounter = zeros(1,N);
       
                z_r_t__tb = zeros(6,N);
                signs = zeros(4,N);
                

                att_ests = cell(1, N);
                
                if obj.constants.PERFECTUAV1
                    gt1 = obj.UAVList{1}.INS.GT;
                    obj.UAVList{1}.INS.r_t__t_b_INS(:,i) = gt1.r_t__t_b{1}(:,i);
                end
                R_cumulative = zeros(1, N);
                R_est_counter = zeros(1, N);

                for cellnum = 1:N
                    att_ests{1, cellnum} = zeros(4);
                end

                for n = 1:N
                    % Estimation of self (from IMU)
                    true_z = zeros(3,N);
                    for o = 1:N
                        gt = obj.UAVList{o}.INS.GT;
                        true_z(:,o) = gt.r_t__t_b{o}(:,max([1, i]));
                    end
                    
                    gt = obj.UAVList{n}.INS.GT;
                    %self_estimation = gt.r_t__t_b{n}(:,max([1, i]));  % GROUND TRUTH
                    self_estimation = obj.UAVList{n}.INS.r_t__t_b_INS(:,max([1, i]));

                    self_attitude = obj.UAVList{n}.INS.C_t__b_INS(:,:,i);
                    self_attitude = math.dcm2q(self_attitude);
                    true_att = math.dcm2q(obj.UAVList{n}.INS.GT.C_t__b{n}(:,:,i));
                    %self_attitude = true_att;

                    %self_attitude = true_att; %%%FIXME

                    signs(:,n) = sign(self_attitude);
                    

                    if ~isreal(self_estimation)
                        stophere = 1;
                    end
                    
                    if obj.constants.USE_SELF
                        z_r_t__tb(4:6,n) = z_r_t__tb(4:6,n) + self_estimation.*w(n);
                        estimationCounter(n) = estimationCounter(n) + 1;
                        att_ests{n} = att_ests{n} + (self_attitude * self_attitude');
                    end

                    
                    % Estimation of neighbors (from ranging sensors)
                    for nn = 1:N
                        % Self is not a neighbor
                        if nn ~= n
                            gt = obj.UAVList{n}.INS.GT;

                            rDiff = gt.r_t__t_b{nn}(:,max([1, i])) - gt.r_t__t_b{n}(:,max([1, i]));
                           
                            D = norm(rDiff);    % True distance between agents
    
                            % Check if neighbor is nearby
                            if D <= obj.constants.distanceThreshold
                                %rDiff_tilde = rDiff + sqrt(obj.constants.neighbor_var).*randn(3,1);
                                
                                C_t__b1 = gt.C_t__b{n}(:,:,max([1, i]));
                                %C_bneighbor__bself = gt.C_t__b{nn}(:,:,i)' * gt.C_t__b{n}(:,:,i);
                                C_t__bn = obj.UAVList{nn}.INS.C_t__b_INS(:,:,i);
                                C_t__bs = obj.UAVList{n}.INS.C_t__b_INS(:,:,i);
                                C_bneighbor__bself = C_t__bn' * C_t__bs;

                                C_b1__t_true = gt.C_t__b{n}(:,:,max([1, i]))';
                                r_b1__b1b2_true = C_b1__t_true * rDiff;

                                R = norm(rDiff);
                                
                                r_initview = gt.r_t__t_b{nn}(:,1) - gt.r_t__t_b{n}(:,1);
                                r_rel = r_b1__b1b2_true;% - r_initview;
                                x = r_rel(1);
                                y = r_rel(2);
                                z = r_rel(3);
                                % theta = wrapToPi(acos(r_rel(3)/R));
                                % phi = atan2(r_rel(2), r_rel(1));

                                azimuth = atan2(y,x);
                                elevation = atan2(z,sqrt(x.^2 + y.^2));

                                phi = abs(wrapToPi(elevation - pi/2));

                                r = sqrt(x.^2 + y.^2 + z.^2);


                                FOV = obj.constants.FOV * pi/180;

                                if phi >= -FOV/2 && phi <= FOV/2
                                    seen = 1;
                                else 
                                    seen = 0;
                                end
                                if nn == 5
                                    stophere = 1;
                                end

                                
                                C_t__b_init = obj.UAVList{n}.INS.GT.C_t__b{n}(:,:,1);
                                [neighbor_estimation, sigma_xyz] = rangingSensor(obj.constants, rDiff, self_estimation, C_t__b_init, C_t__b1, seen);
                                test = obj.UAVList{nn}.INS.GT.r_t__t_b{nn}(:,i);
                              
                                %neighbor_estimation = test;
                                
                                %self_attitude = gt.C_t__b{n}(:,:,i); %%% FIXME
                                
                               
                                [attitudeMeasurement, sigma_psi] = attitudeSensor(obj.constants, rDiff, C_bneighbor__bself, self_attitude, C_t__b_init, C_t__b1, seen);

                                testq = math.dcm2q(obj.UAVList{nn}.INS.GT.C_t__b{nn}(:,:,i));
                                
                                %attitudeMeasurement = true_att; %%%FIXME
                                test = obj.UAVList{n}.INS.GT.C_t__b{n}(:,:,i);


                                % [neighbor_estimation, sigma_xyz] = rangingSensor(rDiff, gt.r_t__t_b{n}(:,max([1,k-1])), C_t__b_init, C_t__b1);
                                % neighbor_estimation = gt.r_t__t_b{n}(:,max([1,k-1])) + rDiff; % GROUND TRUTH 
                                if obj.constants.INSType == "PCATEST"
                                    R_cumulative(nn) = R_cumulative(nn) + sigma_xyz;
                                    R_est_counter(nn) = R_est_counter(nn) + 1;
                                end
                                if ~strcmp(neighbor_estimation, "Damn, it's out of range :(")
                                    if sum(isnan(neighbor_estimation)) ~= 0
                                        stophere = 1;
                                    end
                                    z_r_t__tb(4:6,nn) = z_r_t__tb(4:6,nn) + neighbor_estimation.*w(n);
                                    estimationCounter(nn) = estimationCounter(nn) + 1;
                                end
                                if ~strcmp(attitudeMeasurement, "Damn, it's out of range :(")
                                    if norm(testq - attitudeMeasurement) > 0.5
                                        stophere = 1;
                                    end
                                    % signs = sign(attitudeMeasurement);
                                    att_ests{nn} = att_ests{nn} + (attitudeMeasurement * attitudeMeasurement');
                                end
                                
                                %neighbor_estimation = self_estimation + rDiff_tilde;
                                %neighbor_estimation = obj.UAVList{nn}.INS.r_t__t_b_INS(:,k);
                            end
                        end
                    end
                end
                
                % General Form Consensus vector
                true_z = zeros(3,N);
                for n = 1:N
                    if i >= 850
                        stophere = 1;
                    end

                    if sum(estimationCounter < 1) > 0
                        stophere = 1;
                    end

                    z_r_t__tb(4:6,n) = z_r_t__tb(4:6,n)./(estimationCounter(n));
                    true_z(:,n) = gt.r_t__t_b{n}(:,max([1, i]));
                    
                    [V,D] = eig(att_ests{n});
                    [~,ind] = max(max(D));
                    avgQ = V(:,ind);
                    
                    % test = avgQ - obj.prevQ(:,n);
                    % obj.testprop(i) = norm(test);
                    % if norm(test) > 1 && i ~= 1
                    %     stophere = 1;
                    %     test2 = -1*avgQ;
                    % else
                    %     test2 = avgQ;
                    % end
                    % 
                    % obj.prevQ(:,n) = test2;
                    % %avgQ = signs(:,n) .* abs(avgQ); %%%FIXME
                    % avgQ = test2;
                    % % 
                    % % 
                    % % 
                    % % %avgQ = math.dcm2q(gt.C_t__b{n}(:,:,i));

                    [avgK, avgKm] = math.q2k(avgQ);
                    trueK = math.dcm2k(obj.UAVList{n}.INS.GT.C_t__b{n}(:,:,i));

                    if norm(avgK) > pi
                        %avgK = -1*avgK;
                        %trueK = -1*trueK;

                        avgQ = -avgQ;
                        [avgK, avgKm] = math.q2k(avgQ);

                        stophere = 1;
                    % else
                    %     avgK = avgK .* [1;-1;1];
                    end

                    

                    z_r_t__tb(1:3,n) = avgK; %%% FIXME
                    test = trueK;

                    if norm(avgK - trueK) > .25
                        stophere = 1;
                    end
                    % if norm(trueK) > pi
                    %     z_r_t__tb(1:3,n) = -trueK;
                    % else
                    %     z_r_t__tb(1:3,n) = trueK;
                    % end

                    if obj.constants.INSType == "PCATEST"
                        sigma_xyz = R_cumulative(n)/R_est_counter(n);
                        %sigma_xzy = 100^2;
                        IMU_bias_accounting_a = obj.UAVList{n}.IMU.wk_a;
                        IMU_bias_accounting_g = obj.UAVList{n}.IMU.wk_a;
                        %if (sum(obj.UAVList{n}.IMU.wk_a) == 0) || (sum(obj.UAVList{n}.IMU.wk_g) == 0)
                        IMU_bias_accounting_a = IMU_bias_accounting_a + 1e-4;
                        IMU_bias_accounting_g = IMU_bias_accounting_g + 1e-4;
                        %end

                        R = [sigma_xyz^2, 0, 0;
                             0, sigma_xyz^2, 0;
                             0, 0, sigma_xyz^2] + diag(IMU_bias_accounting_a);
                        Rpsi = [sigma_psi^2, 0, 0;
                             0, sigma_psi^2, 0;
                             0, 0, sigma_psi^2] + diag(IMU_bias_accounting_g);
                        if obj.constants.ATT_AIDING
                            obj.UAVList{n}.INS.kalman.R = [Rpsi, zeros(3); zeros(3), R];
                        else
                            obj.UAVList{n}.INS.kalman.R = R;
                        end
                    end

                end
                if i >= 15000/2 - 1
                    stophere = 1;
                end
                
                % Generate swarm frame
                % [C_s__t, r_s__s_b, C_t__s, r_t__t_s] = getSwarmFrame(z_r_t__tb);
                % obj.GC_PCA.C_s__t = C_s__t;
                % obj.GC_PCA.C_t__s = C_t__s;
                % obj.GC_PCA.r_t__t_s = r_t__t_s;
                % 
                % obj.GC_PCA.C_t__s_array(:,:,k) = C_t__s;
                % obj.GC_PCA.r_t__t_s_array(:, k) = r_t__t_s; 
                % 
                % 
                % 
                % gt = obj.UAVList{1}.INS.GT;
                % C_e__t = gt.C_e__t;
                % C_t__e = C_e__t.';
                % r_e__e_t = gt.r_e__et;
                % 
                % S_star = zeros(size(z_r_t__tb));
                % for n = 1:N
                %     %em.P{n}(:,k) = em.r_e__es{k} + em.C_e__s{k}*r_s__sb(:,n);
                %     %obj.UAVList{n}.INS.r_e__e_b_INS(:,k) = r_e__e_s + C_e__s*r_s__s_b(:,n);
                %     %obj.UAVList{n}.INS.r_t__t_b_INS(:,k) = r_t__t_s + C_t__s*r_s__s_b(:,n);
                %     S_star(:,n) = r_t__t_s + C_t__s*r_s__s_b(:,n);
                % end
                
                if ~isreal(z_r_t__tb)
                    stophere = 1;
                end
                    
                obj.GC_PCA.S_star_minus = obj.GC_PCA.S_star;
                if sum(sum(isnan(z_r_t__tb))) ~= 0
                    stophere = 1;
                end
                if ~obj.constants.PERFECT_CONSENSUS
                    obj.GC_PCA.S_star = z_r_t__tb;
                    obj.est_counter = estimationCounter;
                else
                    obj.GC_PCA.S_star = true_z;
                end


                
                
                if obj.constants.PERFECTUAV1
                    gt1 = obj.UAVList{1}.INS.GT;
                    obj.UAVList{1}.INS.r_t__t_b_INS(:,i) = gt1.r_t__t_b{1}(:,i);
                end

            end
        end
  
    end
end

