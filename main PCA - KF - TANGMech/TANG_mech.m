function [r_t__t_b_out, v_t__t_b_out, C_t__b_out, a_t__t_b] = TANG_mech(constants,r_t__t_b, v_t__t_b, C_t__b, w_b__i_b_tilde, f_b__i_b_tilde)
    
% DESCRIPTION:

%--------------------------------------------------------------------------
% Needed variables
dt = constants.dt;      % Time step
Ohm_e__i_e=constants.Ohm_i__i_e; 
C_e__t = constants.C_e__t;
C_t__e = C_e__t.';
r_e__e_t = constants.r_e__e_t;

Ohm_b__i_b=math.vec2ss(w_b__i_b_tilde);

r_t__e_t = C_e__t.'*r_e__e_t;
r_t__e_b = r_t__e_t + r_t__t_b;
r_e__e_b = r_e__e_t + C_e__t * r_t__t_b;

Ohm_t__i_e = C_e__t.'*Ohm_e__i_e*C_e__t;
Ohm_t__t_b = C_t__b*Ohm_b__i_b*C_t__b.' - Ohm_t__i_e;

w_t__t_b=math.ss2vec(Ohm_t__t_b);

dtheta=norm(w_t__t_b*dt);
k=(w_t__t_b*dt)/dtheta;
Kappa=math.vec2ss(k);

%--------------------------------------------------------------------------
% STEP 1.) Attitude Update
C_t__b_out = (eye(3) + sin(dtheta)*Kappa + (1-cos(dtheta))*Kappa^2)*C_t__b;

%--------------------------------------------------------------------------
% STEP 2.) Specific Force Update
if constants.specificTANGmech
    alpha_cross = math.vec2ss(w_b__i_b_tilde)*constants.dt;
    mag_alpha = norm(math.ss2vec(alpha_cross));
    C_bm__bbar = eye(3)+ (1-cos(mag_alpha))/(mag_alpha^2)*alpha_cross+1/(mag_alpha^2)*(1-(sin(mag_alpha))/(mag_alpha))*alpha_cross*alpha_cross;
    C_t__b_bar = C_t__b*C_bm__bbar - 0.5*(Ohm_t__i_e)*C_t__b*constants.dt;
    f_t__i_b = C_t__b_bar * f_b__i_b_tilde;
else
    f_t__i_b = C_t__b_out*f_b__i_b_tilde;
end

%--------------------------------------------------------------------------
% STEP 3.) Velocity Update
%gamma_t__i_b = nav.gamma__i_b(constants,r_t__e_b);  % r_t__e_b = r_t__i_b
gamma_e__i_b = nav.gamma__i_b(constants,r_e__e_b);
%g_t__b = gamma_t__i_b - Ohm_t__i_e*Ohm_t__i_e*r_t__e_b;
g_e__b = gamma_e__i_b - Ohm_e__i_e*Ohm_e__i_e*r_e__e_b;
g_t__b = C_t__e * g_e__b;
%g_t__b = C_t__e * gamma_e__i_b - Ohm_t__i_e * Ohm_t__i_e*r_t__e_b;
a_t__t_b = (f_t__i_b + g_t__b - 2*Ohm_t__i_e*v_t__t_b);

if constants.specificTANGmech
    v_t__t_b_prime = v_t__t_b + a_t__t_b*dt;
    v_t__t_b_out = v_t__t_b + (f_t__i_b + g_t__b - Ohm_t__i_e*(v_t__t_b + v_t__t_b_prime))*constants.dt;
else
    v_t__t_b_out = v_t__t_b + a_t__t_b*dt;
end
%--------------------------------------------------------------------------
% STEP 4.) Position Update
r_t__t_b_out = r_t__t_b + v_t__t_b_out*dt + a_t__t_b*((dt^2)/2);

stophere = 1;
end