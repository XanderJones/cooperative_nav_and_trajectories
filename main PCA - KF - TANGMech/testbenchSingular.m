clear
echo off

IMU.Orientus

load_constants;

constants.INSType = "GPS_Aided";
constants.IDEALIMU = 1;

trajNum = 1;

N = 15000;
trajectoryFile = './trajFiles/gt_f8_100Hz.mat';
disp('gt')
groundTruth1 = groundTruth(constants, trajectoryFile);
constants.numIMUs = groundTruth1.gt.numIMUs;

constants.C_e__t = groundTruth1.gt.C_e__t;
constants.r_e__e_t = groundTruth1.gt.r_e__et;
constants.imu_const = imu_const;

GC_PCA_handle = GC_PCA(1, groundTruth1.gt);
swarmTemp = Swarm(constants, GC_PCA_handle);

INS = GPS_Aided(trajNum, constants, GC_PCA_handle, groundTruth1.gt, imu_const, groundTruth1.gt.K);
IMU1 = IMU(trajNum, groundTruth1.gt,constants);

UAV1 = UAV(trajNum, IMU1, INS);

for i = 1:N-1
    UAV1 = UAV1.iterate();
    if mod((i/N * 100), 5) == 0
        disp([num2str(i / N * 100), '% complete']);   
    end
end

swarm_tang.UAVList{1} = UAV1;
swarmPlot(constants, swarm_tang,["3DPlot"],3, 1)
