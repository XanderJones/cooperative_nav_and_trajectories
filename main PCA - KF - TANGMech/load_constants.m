% FUNCTION DESCRIPTION:
%   This is a script which defines the constants struct

constants.DEAD_RECKONING = 0;
constants.CLOSED_LOOP = 1;
constants.UNSCENTED = 0;

constants.fidelity = "high";
constants.MAKEWORSEIMU = 0;
constants.IDEALIMU = 0;
constants.displayProgress = 1;
constants.DORENORMALIZE = 0;
constants.DISPCOMPLETION = 1;
constants.renormalizeThreshold = 10^(-2);
constants.indivPlots = 1;
constants.reduceIMUnum = 0;

constants.USE_SELF = 0;

constants.noREL = 0;

constants.FOV = 360;

constants.centerGPS = 0;
constants.allGPS = 0;

constants.ATT_AIDING = 1;
constants.IMU_CALIBRATED = 1;

constants.angularRes = 0.25;

constants.QUATERNIONangularVel = 1;
constants.NOCONSENSUS = 0;
constants.PERFECTUAV1 = 0;
constants.specificTANGmech = 0;
constants.PERFECT_CONSENSUS = 0;

constants.INSType = "PCATEST";

%------------------------------------------------------------------------------
% Sampling rate variables and simulation time
%------------------------------------------------------------------------------
constants.Fs  = 100;                 % Sample frequency (Hz)
constants.dt  = 1/constants.Fs;     % Sample interval (sec)
constants.t_start = 0;              % Simulation start time (sec)
constants.t_end = 300;              % Simulation end time (sec)

constants.F_measure = 100;
constants.F_CLOSED = 100;

constants.N = constants.Fs * (constants.t_end - constants.t_start) + 1;

constants.OUTAGE = 0;
constants.CORRUPT = 0;              % Only designed to accept one of the two for now 
constants.t1 = 30;
constants.t2 = 60;

constants.NOISY = 0;

%------------------------------------------------------------------------------
% GPS model parameters
%------------------------------------------------------------------------------

constants.L_0 = 34.0648;
constants.lambda_0 = -106.9034;
constants.h_0 = 0;

constants.sigma_r = 3;
constants.sigma_v = 10e-2;
constants.F_GPS = 100;

%------------------------------------------------------------------------------
% Earth model parameters
%------------------------------------------------------------------------------
constants.w_ie = 72.92115167e-6;    % WGS84 Earth rate (rad/s)
constants.mu = 3.986004418e14;      % Earth's gravitational constant (m^3/s^2)
constants.J2 = 1.082627e-3;         % Earth's second gravitational constant
constants.R0 = 6378137.0;           % Earth's equatorial radius (meters)
constants.Rp = 6356752.3142;        % Earth's polar radius (meters)
constants.e = 0.0818191908425;      % Eccentricity
constants.f = 1 / 298.257223563;    % Flattening from page 38 of text

constants.Ohm_i__i_e = math.vec2ss([0;0;constants.w_ie]);
