function [bk,wk] = first_order_Markov(sigma_BI,Tc,Fs,bkLast,wkLast)
% FUNCTION DESCRIPTION:
%   Simulates Bias Instability as First Order Markov Noise
%
% INPUTS:
%   sigma_BI = bias instability standard deviation
%   Tc = Correlation time constant [s]
%   Fs = Simulation sample rate [Hz]
%   bkLast = last iteration's First Order Markov Noise value
%   wkLast = last iteration's white noise term
%
% OUTPUTS:
%   bk = this iteration's First Order Markov Noise value
%   wk = this iteration's white noise term

%% Generate the white noise term
Ts = 1/Fs;
Qd = sigma_BI^2*(1 - exp(-2*Ts/Tc));
sigma_w = Qd;
wk = sigma_w*randn(3,1);

%% First order Markov iteration
bk = exp(-Ts/Tc)*bkLast + wkLast;
end
