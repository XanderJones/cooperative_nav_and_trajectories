%close all
clear
echo off


%rng(42)

%IMU.KVH1750
IMU.Orientus
%IMU.STIM300
%IMU.ADIS16500
mont = 1;
currentMont = 1;
POV_VIEW = 1;

load_constants;
constants.PERFECT_CONSENSUS = 0;
constants.IDEALIMU= 0;

constants.CLOSED_LOOP = 1;
constants.UNSCENTED = 0;

numIMUs = 7;

%constants.INSType = "GPS_Aided";
%constants.INSType = "GPS_Consensus_Aided";
constants.INSType = "PCATEST";

constants.ATT_AIDING = 0;


constants.centerGPS = 0;
constants.allGPS = 0;



constants.distanceThreshold = 10;
constants.neighbor_var = 5e-2;
constants.weightingVec = ones(1,numIMUs);

constants.sigma_r = 1;

constants.noREL = 0;
constants.DEAD_RECKONING = 0;

%constants.weightingVec = [2/numIMUs, ones(1,numIMUs-1)*(numIMUs-2)/(numIMUs-1)/numIMUs];

N = 30000;

constants.angularRes = 0.25;
constants.IMU_CALIBRATED = 1;
constants.F_measure = 100;
trajectoryFile = './trajFiles/gt_uav_f8.mat';
numIMUs = 7;
swarm_tang = swarmFactory(imu_const, trajectoryFile, constants, numIMUs);
disp("ITERATING")

for i = 1:N-1

    
    swarm_tang = swarm_tang.updatePCA(i);  
    swarm_tang = swarm_tang.iterate(i);


    if mod((i/N * 100), 5) == 0
        disp([num2str(i / N * 100), '% complete']);   
    end


end
disp("DONE")

swarmPlot(constants, swarm_tang,["3DPlot","RelError","CovPlot"],1, 1);