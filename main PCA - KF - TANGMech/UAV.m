classdef UAV < handle
    % OBJECT DESCRIPTION:
    %   The UAV object serves primarily to allow an INS object and IMU
    %   object to communicate and hopefully be seemlessly swapped between
    %   each other.
    %
    % PROPERTIES:
    %   The INS and IMU properties are object handles to allow property
    %   reading. The f and w arrays are used to store measurements from the
    %   accelerometer and gyroscope for processing later
    %
    % METHODS:
    %   iterate: calls the INS iterate function. This is a bit jank but it
    %   works
    
    properties (SetAccess = private)
        INS
        IMU
        index
        f_array
        w_array
    end
    
    methods
        function UAV = UAV(index, IMU, INS)
            UAV.index = index;
            UAV.IMU = IMU;
            UAV.INS = INS;

        end

        function UAV = iterate(UAV, i)
            imu_const = UAV.INS.imu_const;
            [f_b__i_b_tilde, w_b__i_b_tilde] = UAV.IMU.getMeasurement(imu_const, UAV.index);

            UAV.f_array(:,i) = f_b__i_b_tilde;
            UAV.w_array(:,i) = w_b__i_b_tilde;

            UAV.INS = UAV.INS.iterate(w_b__i_b_tilde, f_b__i_b_tilde, i);
        end


    end
end

