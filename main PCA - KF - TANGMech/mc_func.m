% FUNCTION DESCRIPTION:
%   This is just a quick-ish and easy-ish way to run monte carlo tests
%   without having to rewrite a whole lot
%
% INPUTS:
%   folderPath: where outputs are to be saved
%   mont: how many iterations to run
%   IMUnum: To select which IMU to use (should be made generic)
%   F_meas: measurement frequency
%   trajectoryFile: The folder path to the trajectory you want to use
%   ATT_AIDING: whether attitude aiding is used. Position is always used
%   angRes: The angular resolution of the attitude sensor
%   centerGPS: Whether there is a GPS on the center IMU
%   DEAD_RECKONING: Overwrites all other measurements to do dead reckoning
%   ThreeD: Outdated plotting tool
%   GPSOnly: Use GPS without relative aiding
%   noREL: Disable relative aiding
%   UKF: Whether to use an EKF or a UKF
%   CLOSED_LOOP: Whether its open or closed loop configuration
%   
%
% OUTPUTS:
%   Outputs a plot showing relative and absolute position and orientation
%   errors to the desired folderPath

function mc_func(folderPath, mont, IMUnum, F_meas, trajectoryFile, ATT_AIDING, angRes, centerGPS, DEAD_RECKONING, ThreeD, GPSOnly, noREL, UKF, CLOSED_LOOP)

    close all
    %mont = 3;

    
    
    scenDef.mont = mont;
    scenDef.IMUnum = IMUnum;
    scenDef.F_meas = F_meas;
    scenDef.trajectoryFile = trajectoryFile;
    scenDef.ATT_AIDING = ATT_AIDING;
    scenDef.angRes = centerGPS;
    scenDef.DEAD_RECKONING = DEAD_RECKONING;
    scenDef.noREL = noREL;
    scenDef.UKF = UKF;
    scenDef.CLOSED_LOOP = CLOSED_LOOP;

    swarm_cell = cell(2, mont);
    swarm_cell{2, 1} = mont;
    
    %%% Write to a file
    tic
    for m = 1:mont
    
        clear swarm_current
        switch IMUnum
            case 1
                IMU.KVH1750
            case 3
                IMU.Orientus
            case 2
                IMU.STIM300
            case 4
                IMU.ADIS16500
            otherwise
                error("Invalid IMU selection")
        end
        % IMU.KVH1750
        % % IMU.Orientus
        % % IMU.STIM300
        % PCACounter = 0;

        %trajectoryFile = './trajFiles/IMUS_21.mat';

        load(trajectoryFile)
        [~, numIMUs] = size(gt.C_t__b);
    

    
        load_constants;
    
        constants.INSType = "PCATEST";
        constants.reduceIMUnum = 0;
        constants.F_measure = F_meas;
        constants.distanceThreshold = 100;
        constants.neighbor_var = 5e-2;
        
        constants.DEAD_RECKONING = DEAD_RECKONING;
        constants.ATT_AIDING = ATT_AIDING;
        constants.angularRes = angRes;

        constants.UKF = UKF;
        constants.CLOSED_LOOP = CLOSED_LOOP;

        if GPSOnly
            constants.allGPS = 1;
        end

        if noREL
            constants.noREL = 1;
        else
            constants.noREL = 0;
        end
    
        %numIMUs = 7;
        constants.weightingVec = ones(1,numIMUs);
    
        constants.IMU_CALIBRATED = 0;
    
        constants.centerGPS = centerGPS;
        %constants.sigma_r = 0.01;
    
        load(trajectoryFile)
        [~, b] = size(gt.r_t__t_b{1});
        N = b - 1;

        %N = floor(N / 8); %%%FIXME
        
        % %trajectoryFile = './trajFiles/gt_uav_7stationary.mat';
        % %trajectoryFile = './trajFiles/gt_f8_100Hz.mat';
        % trajectoryFile = './trajFiles/Scurve.mat';
        % %trajectoryFile = './trajFiles/gt_uav_f8.mat';
    
        %numIMUs = 7;
        swarm_current = swarmFactory(imu_const, trajectoryFile, constants, numIMUs);
        disp("ITERATING")
        lastpercent = 0;

        scenDef.constants = constants;

        for i = 1:N-1
            swarm_current = swarm_current.updatePCA(i);
            swarm_current = swarm_current.iterate(i);
            if mod(floor(i/N * 100), 1) == 0 && lastpercent ~= floor(i/N*100)
                disp(['TEST: ', num2str(m), ' of ', num2str(mont), ' is ', num2str(floor(i / N * 100)), '% complete'])
                lastpercent = floor(i/N*100);
            end
        end
    
        CumulativeTestTime = toc;
        AvgTestTime = CumulativeTestTime / m;
        remainingTime = seconds((mont - m) * AvgTestTime);
    
        remainingTime.Format = 'hh:mm:ss';
    
        disp(['TEST: ', num2str(m), ' is DONE - remaining time: ', char(remainingTime)])
        swarm_cell{1, m} = swarm_current;
    end
    
    %save('monte_carlo.mat', 'swarm_cell')
    
    mkdir(folderPath)
    figNames = {"Errors.fig", "ThreeD.fig"};
    
    if ThreeD
        save([folderPath, "scenDef"], "scenDef")
        swarm_cell_process(constants, swarm_cell, 1)
        savefig(figure(1), strjoin([folderPath, figNames{2}],""))
    else
        swarm_cell_process(constants, swarm_cell, 0)
        savefig(figure(1), strjoin([folderPath, figNames{1}],""))
    end


end