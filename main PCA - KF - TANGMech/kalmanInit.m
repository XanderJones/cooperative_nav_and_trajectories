% Kalman Initialization

% Based on the formulation in the thesis

% % Naive approach for camera with some angular resolution
% focal_length = 4.74 * 10^(-3);
% Fnumber = 1.2;
% wavelength = 550 * 10^(-9); % average visible light
% 
% % Using Raspberry Pi Cam 3, wide lens
% apertureDiameter = focal_length/Fnumber;
% angularRes = 1.22 * wavelength / apertureDiameter;

angularRes = constants.angularRes;
sigma_psi = sqrt(angularRes^2/3);

%angularRes = 0.25;
   

sigma_range = constants.neighbor_var;
sigma_xyz = sqrt(sigma_range^2/3); %%% FIXME
R = [sigma_xyz^2, 0, 0;
     0, sigma_xyz^2, 0;
     0, 0, sigma_xyz^2];
Rpsi = [sigma_psi^2, 0, 0;
     0, sigma_psi^2, 0;
     0, 0, sigma_psi^2];
if constants.ATT_AIDING
    kalman.R = [Rpsi, zeros(3); zeros(3), R];
else
    kalman.R = R;
end

Zeros3x3   = zeros(3);
I3x3       = eye(3);

del_psi_mag = constants.imu_const.gyro.b_g_BI_sigma;
del_psi_x = (del_psi_mag)*(pi/180);
del_psi_y = (del_psi_mag)*(pi/180);
del_psi_z = (del_psi_mag)*(pi/180);

del_r_x = sigma_xyz;
del_r_y = sigma_xyz;
del_r_z = sigma_xyz;

del_v_x = sigma_xyz;
del_v_y = sigma_xyz;
del_v_z = sigma_xyz;

del_b_g_x = imu_const.gyro.b_g_BI_sigma;
del_b_g_y = imu_const.gyro.b_g_BI_sigma;
del_b_g_z = imu_const.gyro.b_g_BI_sigma;

del_b_a_x = imu_const.accel.b_a_BI_sigma;
del_b_a_y = imu_const.accel.b_a_BI_sigma;
del_b_a_z = imu_const.accel.b_a_BI_sigma;


del_psi = [del_psi_x ,     0     ,     0     ;
               0     , del_psi_y ,     0     ;
               0     ,     0     , del_psi_z ];

del_v   = [del_v_x ,     0   ,     0   ;
               0   , del_v_y ,     0   ;
               0   ,     0   , del_v_z ];

del_r   = [del_r_x ,    0    ,    0    ;
              0    , del_r_y ,    0    ;
              0    ,    0    , del_r_z ];

del_b_g = [del_b_g_x ,     0     ,     0     ;
               0     , del_b_g_y ,     0     ;
               0     ,     0     , del_b_g_z ];

del_b_a = [del_b_a_x ,     0     ,     0     ;
               0     , del_b_a_y ,     0     ;
               0     ,     0     , del_b_a_z ];
 
kalman.P = [ del_psi , Zeros3x3 , Zeros3x3 , Zeros3x3 , Zeros3x3;
     Zeros3x3 ,   del_v  , Zeros3x3 , Zeros3x3 , Zeros3x3;
     Zeros3x3 , Zeros3x3 ,   del_r  , Zeros3x3 , Zeros3x3;
     Zeros3x3 , Zeros3x3 , Zeros3x3 , del_b_g  , Zeros3x3;
     Zeros3x3 , Zeros3x3 , Zeros3x3 , Zeros3x3 , del_b_a ];

Fs = constants.Fs;
ARW = imu_const.gyro.ARW;                   % Gyro Angle Random Walk (deg/rt-hr)
PSD = (60*ARW)^(2);                         % Gyro PSD (deg/hr)^2/Hz
sigma_g = sqrt(Fs*PSD)*(pi/180)*(1/3600);   % Gyro Standard Deviation (rad/sec) 

VRW = imu_const.accel.VRW;
sigma_a = sqrt(Fs*VRW);

n_r_g   = sigma_g;
n_a_g   = sigma_a;
% n_b_a_d = imu_const.gyro.b_g_BI_sigma;
% n_b_g_d = imu_const.accel.b_a_BI_sigma;
n_b_a_d = 2 * imu_const.accel.b_a_BI_sigma^2 / imu_const.accel.BI.correlation_time;
n_b_g_d = 2 * imu_const.gyro.b_g_BI_sigma^2 / imu_const.gyro.BI.correlation_time;


kalman.Q_cont = [n_r_g^2.*I3x3 ,    Zeros3x3    ,  Zeros3x3 ,      Zeros3x3     ,     Zeros3x3      ;
           Zeros3x3     , n_a_g^2.*I3x3 ,   Zeros3x3 ,      Zeros3x3     ,     Zeros3x3      ;
           Zeros3x3     ,    Zeros3x3    ,  Zeros3x3 ,      Zeros3x3     ,     Zeros3x3      ;
           Zeros3x3     ,    Zeros3x3    ,  Zeros3x3 ,   n_b_a_d^2.*I3x3 ,     Zeros3x3      ;
           Zeros3x3     ,    Zeros3x3    ,  Zeros3x3 ,      Zeros3x3     ,  n_b_g_d^2.*I3x3 ];

%

if constants.ATT_AIDING
    kalman.H = [ I3x3, Zeros3x3, Zeros3x3, Zeros3x3, Zeros3x3;
             Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3]; % Posion and Attitude
else
    kalman.H = [ Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3]; % Position only
end

% x is initialized to zero as there is no errors yet
kalman.x = [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0];

% The discrete Q is initialized to the continuous Q
kalman.disc_Q = kalman.Q_cont;
