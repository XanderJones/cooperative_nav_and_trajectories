classdef PCATEST < INS_Interface
    % OBJECT DESCRIPTION:
    %   PCATEST is an outdated name. This INS implements the General Form
    %   Consensus techniques as seen in Xander's thesis
    %
    % PROPERTIES:
    %   The GC_PCA and IMU properties are handles that reference the
    %   associated objects. The properties ending in _out and _INS refer to
    %   internal and external properties. _INS are updated with the Kalman
    %   filter and _out properties are used when open loop configurations
    %   are desired
    %
    % METHODS:
    %   iterate: updates the _INS and _out values by mechanization and
    %   incorporates kalman corrections
    %                                                                     
    %   updateKalman: performs shared Kalman computations and calls either 
    %   the EKF or UKF methods.
    % 
    %   EKF: Runs the Extended Kalman Filter calculations
    % 
    %   UKF: Runs the Unscented Kalman Filter calculations
    
    properties
        counter double = 1
    end
    properties (SetAccess = immutable)
        index
    end
    properties (SetAccess = public)
        GT
        GC_PCA

        tau

        constants
        
        IMU
        imu_const

        PCA_Update = 0

        r_t__t_b_INS
        v_t__t_b_INS%    <--- These must be implemented but are unused
        C_t__b_INS

        r_t__t_b_out
        v_t__t_b_out
        C_t__b_out

        a_array
            
        t
        tGPS
        tCLOSED
        dtCLOSED
        GPSFreq
        prevT
        CLOSED_FLAG

        kalman
        f_b__i_b

        P_hist_psi
        P_hist_xyz

        residuals

        dt_meas

    end

    methods
        function obj = PCATEST(index, constants, GC_PCA, gt, imu_const, length)
            
            % Object Initialization

            obj.index = index;
            obj.constants = constants;
            obj.GC_PCA = GC_PCA;
            obj.imu_const = imu_const;
            obj.t = 0;
            obj.tGPS = 0;
            
            obj.tCLOSED = 0;
            obj.GPSFreq = 0;
            obj.prevT = 0;
            obj.CLOSED_FLAG = 1;
            obj.dtCLOSED = 1/obj.constants.F_CLOSED;

            if obj.index == 1 && obj.constants.centerGPS
                obj.constants.F_measure = 10; %%%FIXME
            end

            obj.dt_meas = 1/obj.constants.F_measure;

            K = length;

            obj.GT=gt;
            %index = 1;
            obj.r_t__t_b_INS = repmat(gt.r_t__t_b{index}(:,1),[1 K]);
            obj.v_t__t_b_INS = repmat(gt.v_t__t_b{index}(:,1),[1 K]);
            obj.C_t__b_INS = repmat(gt.C_t__b{index}(:,:,1),[1 1 K]);

            obj.r_t__t_b_out = repmat(gt.r_t__t_b{index}(:,1),[1 K]);
            obj.v_t__t_b_out = repmat(gt.v_t__t_b{index}(:,1),[1 K]);
            obj.C_t__b_out = repmat(gt.C_t__b{index}(:,:,1),[1 1 K]);

            obj.P_hist_psi = zeros(size(obj.C_t__b_INS));
            obj.P_hist_psi(:,:,:) = NaN;

            obj.P_hist_xyz = zeros(size(obj.C_t__b_INS));
            obj.P_hist_xyz(:,:,:) = NaN;
            
            kalmanInit;
            obj.kalman = kalman;
        end

        function obj = updatePCA(obj, PCA)
            obj.PCA_Update = PCA;
        end
        
        function obj = iterate(obj, w_b__i_b, f_b__i_b, i)


            

            %obj.r_t__t_b_INS(:,i) = obj.GC_PCA.S_star(:,obj.index); % FIXME

            P = obj.r_t__t_b_INS(:,i);
            V = obj.v_t__t_b_INS(:,i);
            A = obj.C_t__b_INS(:,:,i);

            if ~isreal(P)
                stophere = 1;
            end

            [r_t__t_b_plus, v_t__t_b_plus, C_t__b_plus, a_t__t_b] = TANG_mech(obj.constants,P,V,A,w_b__i_b,f_b__i_b); 

            if ~isreal(r_t__t_b_plus)
                stophere = 1;
            end


            

            % deltaC = math.aa2dcm(obj.kalman.x(1:3));

            
            
            % obj.C_t__b_INS(:,:,i+1) = math.update_att(obj.kalman.x, C_t__b_plus);
            %obj.C_t__b_INS(:,:,i+1) = C_t__b_plus;
            
            %deltaPsi = math.vec2ss(obj.kalman.x(1:3));
            %deltaC = eye(3) - deltaPsi;
            %Chat = (eye(3) - deltaPsi) * C_t__b_plus;
            if obj.constants.ATT_AIDING
                if obj.kalman.x(1) ~= 0
                    stophere = 1;
                end
                %obj.C_t__b_INS(:,:,i+1) = transpose(deltaC) * C_t__b_plus;
                q_current = math.dcm2q(C_t__b_plus);

                dq = math.k2q(obj.kalman.x(1:3));

                q_upd = math.q1xq2(dq, q_current);
                q_upd = q_upd./norm(q_upd);

                C_upd = math.q2dcm(q_upd);
                
                if obj.constants.CLOSED_LOOP && obj.CLOSED_FLAG
                    obj.C_t__b_INS(:,:,i+1) = C_upd;
                    obj.kalman.x(1:3,1) = [0;0;0];
                else
                    obj.C_t__b_INS(:,:,i+1) = C_t__b_plus;
                    obj.C_t__b_out(:,:,i+1) = C_upd;
                end
                
            else
                obj.C_t__b_INS(:,:,i+1) = C_t__b_plus;
            end
            if abs(1-norm(obj.C_t__b_INS(:,:,i+1))) > 10e-6
                obj.C_t__b_INS(:,:,i+1) = math.q2dcm(math.normalize(math.dcm2q(obj.C_t__b_INS(:,:,i+1))));
            end
            
            if sum(obj.kalman.x ~= 0)
                stophere = 1;
            end
            
            if obj.constants.CLOSED_LOOP && obj.CLOSED_FLAG
                obj.r_t__t_b_INS(:,i+1) = r_t__t_b_plus + obj.kalman.x(7:9, 1);
                obj.v_t__t_b_INS(:,i+1) = v_t__t_b_plus + obj.kalman.x(4:6, 1);
                
                obj.kalman.x(4:6,1) = 0;
                obj.kalman.x(7:9,1) = [0;0;0];

                obj.CLOSED_FLAG = 0;
                obj.tCLOSED = obj.tCLOSED - obj.dtCLOSED;
            else
                obj.r_t__t_b_INS(:,i+1) = r_t__t_b_plus;
                obj.v_t__t_b_INS(:,i+1) = v_t__t_b_plus;

                obj.r_t__t_b_out(:,i+1) = obj.r_t__t_b_INS(:,i+1) + obj.kalman.x(7:9);
                obj.v_t__t_b_out(:,i+1) = obj.v_t__t_b_INS(:,i+1) + obj.kalman.x(4:6);
            end

            obj.a_array(:,i+1) = a_t__t_b;
            
            %obj.kalman.x(:,1) = 0;

            obj.f_b__i_b = f_b__i_b;
            %obj.r_t__t_b_INS(:,i) = obj.GC_PCA.S_star(:,obj.index); % FIXME


            % obj = obj.updateKalman(i);
            dtmeasure = 1/obj.constants.F_measure;
            dtGPS = 1/obj.constants.F_GPS;
            obj.dtCLOSED = 1/obj.constants.F_CLOSED;
            obj.t = round(obj.t + obj.constants.dt,10);
            obj.tGPS = round(obj.tGPS + obj.constants.dt,10);
            obj.tCLOSED = round(obj.tCLOSED + obj.constants.dt, 10);
            
            if obj.tGPS >= dtGPS
                obj.GPSFreq = 1;   
            end
            if obj.tCLOSED >= obj.dtCLOSED
                obj.CLOSED_FLAG = 1;
            end
            if obj.t >= dtmeasure || obj.tGPS >= dtGPS %|| i == 1
                obj.tau = obj.t;
                if 1/obj.constants.F_measure ~= round(obj.tau,4)
                    stophere = 1;
                end
                if ~obj.constants.DEAD_RECKONING
                    obj = obj.updateKalman(i);
                    if obj.GPSFreq
                        obj.GPSFreq = 0;
                        obj.tGPS = obj.tGPS - dtGPS;
                    end
                end
                obj.prevT = obj.t;
                obj.t = obj.t - dtmeasure;
            end

            
            

            
            
        end

        function obj = updateKalman(obj, i)

            if i >= 7500% && obj.index == 1
                stophere = 1;
            end

            if obj.index == 5
                stophere = 1;
            end

            r_meas_att = 1.*obj.GC_PCA.S_star(1:3, obj.index);
            r_meas_pos = 1.*obj.GC_PCA.S_star(4:6, obj.index);

            

            if (obj.constants.centerGPS && obj.index == 1 || (obj.constants.allGPS && obj.GPSFreq)) && ~obj.constants.noREL
                GPS_p = obj.GT.r_t__t_b{obj.index}(:, i) + obj.constants.sigma_r * randn(3, 1);
                GPS_v = obj.GT.v_t__t_b{obj.index}(:, i) + obj.constants.sigma_v * randn(3, 1);

                
                sigma_xyz2 = obj.kalman.R(end, end);

                I3x3 = eye(3);
                Zeros3x3 = zeros(3);
               
                if obj.constants.ATT_AIDING
                    sigma_psi2 = obj.kalman.R(1,1);
                    obj.kalman.R = zeros(12);
                    obj.kalman.R(1:3, 1:3) = sigma_psi2 * eye(3);
                    obj.kalman.R(4:6, 4:6) = sigma_xyz2 * eye(3);
                    obj.kalman.R(7:9,7:9) = obj.constants.sigma_r^2 * eye(3); 
                    obj.kalman.R(10:12,10:12) = obj.constants.sigma_v^2 * eye(3); 
                    obj.kalman.H = [ I3x3, Zeros3x3, Zeros3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, I3x3, Zeros3x3, Zeros3x3, Zeros3x3];
                else
                    obj.kalman.R = zeros(9);
                    obj.kalman.R(1:3, 1:3) = sigma_xyz2 * eye(3);
                    obj.kalman.R(4:6,4:6) = obj.constants.sigma_r^2 * eye(3);
                    obj.kalman.R(7:9,7:9) = obj.constants.sigma_v^2 * eye(3);
                    obj.kalman.H = [ Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, I3x3, Zeros3x3, Zeros3x3, Zeros3x3];
                end
                
            else
                r_meas_pos = 1.*obj.GC_PCA.S_star(4:6, obj.index);

                I3x3 = eye(3);
                Zeros3x3 = zeros(3);
                sigma_xyz2 = obj.kalman.R(end, end);

                if obj.constants.ATT_AIDING
                    sigma_psi2 = obj.kalman.R(1,1);
                    obj.kalman.R = zeros(6);
                    obj.kalman.R(1:3, 1:3) = sigma_psi2 * eye(3);
                    obj.kalman.R(4:6,4:6) = sigma_xyz2 * eye(3); 
                    obj.kalman.H = [ I3x3, Zeros3x3, Zeros3x3, Zeros3x3, Zeros3x3;
                                     Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3];
                else
                    obj.kalman.R = zeros(3);
                    obj.kalman.R(1:3,1:3) = sigma_xyz2 * eye(3);
                    obj.kalman.H = [ Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3];
                end
            end

            if obj.constants.noREL
                GPS_p = obj.GT.r_t__t_b{obj.index}(:, i) + obj.constants.sigma_r * randn(3, 1);
                GPS_v = obj.GT.v_t__t_b{obj.index}(:, i) + obj.constants.sigma_v * randn(3, 1);
                I3x3 = eye(3);
                Zeros3x3 = zeros(3);
                
                obj.kalman.R = zeros(6);
                obj.kalman.R(1:3,1:3) = obj.constants.sigma_r^2 * eye(3); 
                obj.kalman.R(4:6,4:6) = obj.constants.sigma_v^2 * eye(3); 
                obj.kalman.H = [ Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                 Zeros3x3, I3x3, Zeros3x3, Zeros3x3, Zeros3x3];
            end

            %r_meas = obj.GT.r_t__t_b{obj.index}(:,obj.counter);

            % The z measurement vector is the GPS velocity and position minues
            % the estimated velocity and position 
            
            
            
            if (obj.constants.centerGPS && obj.index == 1 || (obj.constants.allGPS && obj.GPSFreq)) && ~obj.constants.noREL

                if obj.constants.ATT_AIDING
                    r_meas = [r_meas_att; r_meas_pos; GPS_p; GPS_v];
                else 
                    r_meas = [r_meas_pos; GPS_p; GPS_v];
                end

                if obj.constants.ATT_AIDING
                    obj.kalman.z = r_meas - [math.dcm2k(obj.C_t__b_INS(:,:,i)); obj.r_t__t_b_INS(:,i); obj.r_t__t_b_INS(:,i); obj.v_t__t_b_INS(:,i)];
                else
                    obj.kalman.z = r_meas - [obj.r_t__t_b_INS(:,i); obj.r_t__t_b_INS(:,i); obj.v_t__t_b_INS(:,i)];
                end

            else

                if obj.constants.ATT_AIDING
                    r_meas = [r_meas_att; r_meas_pos];
                else 
                    r_meas = r_meas_pos;
                end

                if obj.constants.ATT_AIDING
                    obj.kalman.z = r_meas - [math.dcm2k(obj.C_t__b_INS(:,:,i)); obj.r_t__t_b_INS(:,i)];
                else
                    obj.kalman.z = r_meas - obj.r_t__t_b_INS(:,i);
                end

            end
            
            % This checks if a UAV has no measurements made in the relative
            % and has no GPS measurements
            if obj.constants.noREL || (sum(isnan(obj.kalman.z)) ~= 0 &&  (obj.constants.centerGPS && obj.index == 1 || (obj.constants.allGPS && obj.GPSFreq)))
                r_meas = [GPS_p; GPS_v];
                obj.kalman.z = r_meas - [obj.r_t__t_b_INS(:,i); obj.v_t__t_b_INS(:,i)];
                GPS_p = obj.GT.r_t__t_b{obj.index}(:, i) + obj.constants.sigma_r * randn(3, 1);
                GPS_v = obj.GT.v_t__t_b{obj.index}(:, i) + obj.constants.sigma_v * randn(3, 1);
                I3x3 = eye(3);
                Zeros3x3 = zeros(3);
                
                obj.kalman.R = zeros(6);
                obj.kalman.R(1:3,1:3) = obj.constants.sigma_r^2 * eye(3); 
                obj.kalman.R(4:6,4:6) = obj.constants.sigma_v^2 * eye(3); 
                obj.kalman.H = [ Zeros3x3, Zeros3x3, I3x3, Zeros3x3, Zeros3x3;
                                 Zeros3x3, I3x3, Zeros3x3, Zeros3x3, Zeros3x3];
            end

            complexTEST = sum(sum(isnan(obj.kalman.z)));
            skipthisrunplease = 0;
            if (complexTEST ~= 0) && ~(obj.constants.centerGPS && obj.index == 1 || (obj.constants.allGPS && obj.GPSFreq))
                skipthisrunplease = 1;
            end
                
            % obj.kalman.z = r_meas - [math.dcm2k(obj.GT.C_t__b{obj.index}(:,:,i)); obj.r_t__t_b_INS(:,i)];
            % obj.kalman.z = [z_att, r_meas(4:6) - obj.r_t__t_b_INS(:,i)];


            % This is more accurately the estimated force error
            VRW = obj.imu_const.accel.VRW;
            ESTf_b__i_b = obj.f_b__i_b - [VRW; VRW; VRW];

            % Derive the estimated latitude from the estimated position
            r_e__e_b_INS = obj.GT.r_e__et + obj.GT.C_e__t*obj.r_t__t_b_INS(:, i);
            if obj.index == 5 && i ==995
                stophere = 1;
            end
            [ESTL_b, ~, ~] = nav.xyz2llhTANG(obj.constants, obj.r_t__t_b_INS(:,i));
            % Use our functions to derive the F and G matrices from our
            % calculated values
            %C_e__b_INS = obj.GT.C_e__t*obj.C_t__b_INS(:,:,i);
            
            if ~skipthisrunplease
                if obj.constants.UKF
                    obj = obj.UKF(i, ESTf_b__i_b, ESTL_b, r_e__e_b_INS);
                else
                    obj = obj.EKF(i, ESTf_b__i_b, ESTL_b, r_e__e_b_INS);
                end
            end

            % obj.kalman.F = nav.F_matrix(obj.constants, obj.imu_const, obj.C_t__b_INS(:,:,i), ESTf_b__i_b, ESTL_b, r_e__e_b_INS, obj.GT.C_e__t);
            % obj.kalman.G = nav.G_matrix(obj.C_t__b_INS(:,:,i));
            % 
            % % Discretize F and update P
            % %obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            % if 1/obj.constants.F_measure ~= round(obj.tau,4)
            %     stophere = 1
            % end
            % obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            % obj.kalman.disc_Q = nav.iterate_Q(1/obj.constants.F_measure, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            % % obj.kalman.disc_Q = nav.iterate_Q(obj.tau, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            % obj.kalman.P = obj.kalman.disc_Q + obj.kalman.Phi * obj.kalman.P * transpose(obj.kalman.Phi);
            % 
            % obj.P_hist_psi(:,:,i) = obj.kalman.P(1:3,1:3);
            % obj.P_hist_xyz(:,:,i) = obj.kalman.P(7:9,7:9);
            % 
            % % Using our updated matrices, update our Kalman gain
            % try
            %     obj.kalman.K = obj.kalman.P * transpose(obj.kalman.H) / (obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R);
            % catch
            %     stophere = 1;
            % end
            % if cond(obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R) > 1 * 10^6
            %     obj.kalman.K = zeros(15, 6);
            %     disp("SINGULAR - discard run.")
            % end
            % 
            % % Use the Kalman gain to update x and P
            % obj.kalman.x = obj.kalman.K * obj.kalman.z;
            % obj.kalman.P = ( eye(15) - obj.kalman.K * obj.kalman.H) * obj.kalman.P * transpose( eye(15) - obj.kalman.K * obj.kalman.H) + obj.kalman.K * obj.kalman.R * transpose(obj.kalman.K);   
            % 
            % obj.residuals(i) = obj.kalman.P(3, 3);

            if i == 10000
                stophere = 1;
            end

        end
    
        function obj = EKF(obj, i, ESTf_b__i_b, ESTL_b, r_e__e_b_INS)
            obj.kalman.F = nav.F_matrix(obj.constants, obj.imu_const, obj.C_t__b_INS(:,:,i), ESTf_b__i_b, ESTL_b, r_e__e_b_INS, obj.GT.C_e__t);
            obj.kalman.G = nav.G_matrix(obj.C_t__b_INS(:,:,i));

            % Discretize F and update P
            %obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            if 1/obj.constants.F_measure ~= round(obj.tau,4)
                stophere = 1;
            end
            obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            obj.kalman.disc_Q = nav.iterate_Q(1/obj.constants.F_measure, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            % obj.kalman.disc_Q = nav.iterate_Q(obj.tau, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            obj.kalman.P = obj.kalman.disc_Q + obj.kalman.Phi * obj.kalman.P * transpose(obj.kalman.Phi);

            obj.P_hist_psi(:,:,i) = obj.kalman.P(1:3,1:3);
            obj.P_hist_xyz(:,:,i) = obj.kalman.P(7:9,7:9);

            % Using our updated matrices, update our Kalman gain
            try
                obj.kalman.K = obj.kalman.P * transpose(obj.kalman.H) / (obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R);
            catch
                stophere = 1;
            end
            if cond(obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R) > 1 * 10^6
                obj.kalman.K = zeros(15, 6);
                disp("SINGULAR - discard run.")
            end
            
            % Use the Kalman gain to update x and P
            obj.kalman.x = obj.kalman.K * obj.kalman.z;
            obj.kalman.P = ( eye(15) - obj.kalman.K * obj.kalman.H) * obj.kalman.P * transpose( eye(15) - obj.kalman.K * obj.kalman.H) + obj.kalman.K * obj.kalman.R * transpose(obj.kalman.K);   
            
            obj.residuals(i) = obj.kalman.P(3, 3);
        end
    
        function obj = UKF(obj, i, ESTf_b__i_b, ESTL_b, r_e__e_b_INS)
        
            % [~,numUAVs] = size(r_t__t_b_INS);
            % r_t__t_b_aided = zeros(3,numUAVs);
            % for k=1:numUAVs
            %     r_t__t_b_aided(:,k)  = r_t__t_b_INS{1,k}(:,i);
            % end
            
            states       = height(obj.kalman.x);
            observations = height(obj.kalman.z);
            
            alpha        = 10e-3;  % 1e-3 ???
            beta         = 2;  % optimal value
            kappa        = 0;
            nsp          = states*2 + 1;
            
            % Recalculate kappa according to scaling parameters
            kappa = alpha^2*(states+kappa)-states;
            % xPredSigmaPts = zeros(states,nsp);
            % zPredSigmaPts = zeros(observations,nsp);
            
            % Error Mechanization
            obj.kalman.F = nav.F_matrix(obj.constants, obj.imu_const, obj.C_t__b_INS(:,:,i), ESTf_b__i_b, ESTL_b, r_e__e_b_INS, obj.GT.C_e__t);
            obj.kalman.G = nav.G_matrix(obj.C_t__b_INS(:,:,i));

            obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            obj.kalman.disc_Q = nav.iterate_Q(1/obj.constants.F_measure, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            
            PPred   = zeros(size(obj.kalman.disc_Q));
            PxzPred = zeros(height(obj.kalman.x),height(obj.kalman.z));
            S = zeros(size(obj.kalman.R));
            
            
            %obj.kalman.x = obj.kalman.x.*obj.state_history;
            
            % Calculate matrix square root of weighted covariance matrix

            P_diag = diag(obj.kalman.P);
            zero_els = ~find(P_diag);
            if sum(zero_els) ~= 0
                stophere = 1;
            end
            
            try
                Psqrtm = (chol((states+kappa)*obj.kalman.P))';
            catch
                stophere = 1;
                error("Unscented Degradation")
            end
            
            % Array of the sigma points
            xSigmaPts = [zeros(size(obj.kalman.P,1),1) -Psqrtm Psqrtm];
            
            % Add mean back in
            xSigmaPts = xSigmaPts + repmat(obj.kalman.x,1,nsp);
            
            % Array of the weights for each sigma point
            wSigmaPts = [kappa 0.5*ones(1,nsp-1) 0]/(states+kappa);
            
            % Now calculate the zero'th covariance term weight
            wSigmaPts(nsp+1) = wSigmaPts(1) + (1-alpha^2) + beta;
            
            % Duplicate wSigmaPts into matrix for code speedup
            wSigmaPts_xmat = repmat(wSigmaPts(1:nsp),states,1);
            wSigmaPts_zmat = repmat(wSigmaPts(1:nsp),observations,1);
            
            % State model
            %xPredSigmaPts = gpsrange_state_model(Phi,xSigmaPts,zeros(states,nsp));

            xPredSigmaPts = obj.kalman.Phi * xSigmaPts;
            
            % Measurement model
            %zPredSigmaPts = gpsrange_meas_model(H_gps,H_range,r_t__t_b_aided,xPredSigmaPts);
            zPredSigmaPts = obj.kalman.H * xPredSigmaPts;
            
            xPred = sum(wSigmaPts_xmat .* xPredSigmaPts,2);
            zPred = sum(wSigmaPts_zmat .* zPredSigmaPts,2);
            
            exSigmaPt = xPredSigmaPts-repmat(xPred,1,nsp);
            ezSigmaPt = zPredSigmaPts-repmat(zPred,1,nsp);
            
            for i = 2:nsp
                PPred   = PPred   + wSigmaPts(i)*(exSigmaPt(:,i)*exSigmaPt(:,i)');
                PxzPred = PxzPred + wSigmaPts(i)*(exSigmaPt(:,i)*ezSigmaPt(:,i)');
                S       = S       + wSigmaPts(i)*(ezSigmaPt(:,i)*ezSigmaPt(:,i)');
            end
            
            PPred   = PPred   + wSigmaPts(nsp+1)*(exSigmaPt(:,1)*exSigmaPt(:,1)') + obj.kalman.disc_Q;
            PxzPred = PxzPred + wSigmaPts(nsp+1)*(exSigmaPt(:,1)*ezSigmaPt(:,1)');
            S       = S       + wSigmaPts(nsp+1)*(ezSigmaPt(:,1)*ezSigmaPt(:,1)') + obj.kalman.R;
            
            %%%%% MEASUREMENT UPDATE
            % Calculate Kalman gain
            obj.kalman.K  = PxzPred / S;
            
            % Calculate Innovation
            inovation = obj.kalman.z - zPred;
            
            % Update mean
            obj.kalman.x = xPred + obj.kalman.K*inovation;
            
            % Update covariance
            obj.kalman.P = PPred - obj.kalman.K*S*obj.kalman.K';
            if sum(sum(isnan(obj.kalman.P))) ~= 0 
                stophere = 1;
            end


        end
    end
end

