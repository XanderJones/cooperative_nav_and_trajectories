classdef (Abstract) INS_Interface < handle
    %   INS_INTERFACE
    %   The INS_Interface class defines the general behavior of an INS
    %   class in this project.

    %   It's purpose is to abstract an INS into storing EXCLUSIVELY the
    %   current and historical position, velocity, and attitude of some 
    %   agent over time.

    %   The only method that should be called externally is the iterate()
    %   method. Regardless of any aiding, this should work and reference
    %   any needed objects with the associated key.

    %   As with the rest of this project, this is ONLY in the ECEF frame.
    
    properties (Abstract, SetAccess = public)
        r_t__t_b_INS (3, :) double
        v_t__t_b_INS (3, :) double
        C_t__b_INS (3, 3, :) double
    end
    
    methods (Abstract)
        iterate(obj)
    end
end

