function [Phi,Q] = Q_discrete(Q_cont,F,G,tau,numIMUs)

%testQdisc = eye(15*numIMUs)
%testQdisc2 = F.*tau

%=============================
Phi = eye(15*numIMUs) + F.*tau;
%=============================

Q = (1/2).*(Phi*G*Q_cont*G.'*Phi.' + G*Q_cont*G.').*tau;

end