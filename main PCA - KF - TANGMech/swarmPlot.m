function swarm = swarmPlot (constants, swarm, argArray, varargin)
    
% FUNCTION DESCRIPTION:
%   Yikes this a bloated function. This produces outputs from a single
%   test. Given the desired outputs, plots are generated. This needs a full
%   rework so I am not going to type it all out.
%

    %close all
    try %#ok<TRYNC>
        UAVSelect = varargin{1};
    end
    
    suppressOUT = 0;
    try %#ok<TRYNC>
        suppressOUT = varargin{2};
    end
    UAV_List = swarm.UAVList;
    numIMUs = numel(UAV_List);
    if constants.CLOSED_LOOP
        P_temp = UAV_List{1}.INS.r_t__t_b_INS;
    else
        P_temp = UAV_List{1}.INS.r_t__t_b_out;
    end
    N = size(P_temp, 2);
    sumError = zeros(1, N);
    errortmp = zeros(1, N);
    for k = 2:numIMUs
        %P = UAV_List{1}.INS.GT.r_t__t_b{1} - UAV_List{k}.INS.r_t__t_b_INS;
        
        if constants.CLOSED_LOOP
            P = (UAV_List{1}.INS.r_t__t_b_INS - UAV_List{k}.INS.r_t__t_b_INS);
            P_true = (UAV_List{1}.INS.GT.r_t__t_b{1} - UAV_List{k}.INS.GT.r_t__t_b{k});
        else
            P = (UAV_List{1}.INS.r_t__t_b_out - UAV_List{k}.INS.r_t__t_b_out);
            P_true = (UAV_List{1}.INS.GT.r_t__t_b{1} - UAV_List{k}.INS.GT.r_t__t_b{k});
        end

        %true_P = P(:, 1);
        
        %sumError(:, :) = sumError(:, :) + vecnorm(randn(3, 15001)); % - norm(true_P);

        %errortmp = errortmp + vecnorm(P).^2;
        errortmp = errortmp + vecnorm(P-P_true).^2;
    end
    errortmp = (1/(numIMUs - 1) * errortmp);
    
    % MSE_pos_x = 1/(numIMUs - 1) .* sumError(1,:);
    % MSE_pos_y = 1/(numIMUs - 1) .* sumError(2,:);
    % MSE_pos_z = 1/(numIMUs - 1) .* sumError(3,:);
    % RMSE_pos_x = sqrt(MSE_pos_x);
    % RMSE_pos_y = sqrt(MSE_pos_y);
    % RMSE_pos_z = sqrt(MSE_pos_z);
    % swarm.PosError = [RMSE_pos_x; RMSE_pos_y; RMSE_pos_z];
    swarm.PosError = errortmp;
    %swarm.PosError = RMSEtmp;
    % swarm.MSE = MSE;
    % swarm.RMSE = RMSE;
    N = size(UAV_List{1}.INS.C_t__b_INS,3);
    psi_mag = zeros(1, N);
    psi_mag_all = zeros(numIMUs, N);
    for k = 2:numIMUs
        if constants.CLOSED_LOOP
            C_1 = UAV_List{1}.INS.GT.C_t__b{1};
            C_n = UAV_List{k}.INS.C_t__b_INS;
        else
            C_1 = UAV_List{1}.INS.GT.C_t__b{1};
            C_n = UAV_List{k}.INS.C_t__b_out;
        end
        %C_truth = C_1(:,:,1).'*C_n(:,:,1);
        for o = 1:size(C_1, 3)
            %C_1__n = C_1(:,:,o).'*C_n(:,:,o);
            %deltaC = C_truth*C_1__n.';
            deltaC = C_1(:,:,o) * C_n(:,:,o)';
            %deltaC = C_1(:,:,o) * C_n(:,:,o)';
            psi_err_cross = deltaC-eye(3);
            psi = [psi_err_cross(3,2); psi_err_cross(1,3); psi_err_cross(2,1)];
            psi_mag(o) = norm(psi);
        end
        psi_mag_all(k, :) = psi_mag;
    end
    sumError = sum(psi_mag_all);
    swarm.OriError = sumError;


    %%% ABSOLUTE ERROR %%%%
    if constants.CLOSED_LOOP
        P_temp = UAV_List{1}.INS.r_t__t_b_INS;
    else
        P_temp = UAV_List{1}.INS.r_t__t_b_out;
    end
    N = size(P_temp, 2);
    sumError = zeros(1, N);
    for k = 1:numIMUs
        if constants.CLOSED_LOOP
            P = UAV_List{k}.INS.r_t__t_b_INS;
            true_P = UAV_List{k}.INS.GT.r_t__t_b{k};
        else
            P = UAV_List{k}.INS.r_t__t_b_out;
            true_P = UAV_List{k}.INS.GT.r_t__t_b{k};
        end
        
        sumError(1, :) = sumError(1, :) + vecnorm(P - true_P);
        
    end
    MSE = 1/(numIMUs) .* sumError.^2;
    RMSE = sqrt(MSE);
    swarm.AbsPosError = sumError;
    swarm.AbsMSE = MSE;
    swarm.AbsRMSE = RMSE;



    N = size(UAV_List{1}.INS.C_t__b_INS,3);
    psi_mag = zeros(1, N);
    psi_mag_all = zeros(numIMUs, N);
    for k = 1:numIMUs
        if constants.CLOSED_LOOP
            C_n_true = UAV_List{k}.INS.GT.C_t__b{k};
            C_n = UAV_List{k}.INS.C_t__b_INS;
        else
            C_n_true = UAV_List{k}.INS.GT.C_t__b{k};
            C_n = UAV_List{k}.INS.C_t__b_out;
        end
        %C_truth = C_1(:,:,1).'*C_n(:,:,1);
        for o = 1:size(C_1, 3)
            %C_1__n = C_1(:,:,o).'*C_n(:,:,o);
            %deltaC = C_truth*C_1__n.';
            deltaC = C_n_true(:,:,o) * C_n(:,:,o)';
            psi_err_cross = deltaC-eye(3);
            psi = [psi_err_cross(3,2); psi_err_cross(1,3); psi_err_cross(2,1)];
            psi_mag(o) = norm(psi);
        end
        psi_mag_all(k, :) = psi_mag;
    end
    sumError = sum(psi_mag_all);
    swarm.AbsOriError = sumError;
    %%% ABSOLUTE ERROR %%%%





    dirName = string(datetime);
    dirName = strjoin([argArray, dirName]);
    dirName = strrep(dirName, ' ', '_');
    dirName = strrep(dirName, ':', '-');
    dirName = sprintf("./figures/%s", dirName);
    if ~suppressOUT
        dirName = string(datetime);
        dirName = strjoin([argArray, dirName]);
        dirName = strrep(dirName, ' ', '_');
        dirName = strrep(dirName, ':', '-');
        dirName = sprintf("./figures/%s", dirName);
        mkdir(dirName)
    end
    for i = 1:length(argArray)
        currentArg = argArray(i);
        UAV_List = swarm.UAVList;
        numIMUs = numel(UAV_List);
        
        for j = 1:numIMUs
            UAVtemp = UAV_List{j};
            m = UAVtemp.index;
            GTList{j,1} = UAVtemp.INS.GT.r_t__t_b{m};
            GTList{j,2} = UAVtemp.INS.GT.v_t__t_b{m};
            GTList{j,3} = UAVtemp.INS.GT.C_t__b{m};
        end
        switch currentArg
            case "PVAAll"
                for k = 1:numIMUs
                    P_truth = GTList{k,1};
                    V_truth = GTList{k,2};
                    A_truth = GTList{k,3};
                    if constants.CLOSED_LOOP
                        P_est = UAV_List{k}.INS.r_t__t_b_INS;
                        V_est = UAV_List{k}.INS.v_t__t_b_INS;
                        A_est = UAV_List{k}.INS.C_t__b_INS;
                    else
                        P_est = UAV_List{k}.INS.r_t__t_b_out;
                        V_est = UAV_List{k}.INS.v_t__t_b_out;
                        A_est = UAV_List{k}.INS.C_t__b_out;
                    end
                    [~] = plot_PVA(constants, P_truth, V_truth, A_truth, P_est, V_est, A_est, "TANG", k);
                end
                
            case "PVAOne"
                k = UAVSelect;
                P_truth = GTList{k,1};
                V_truth = GTList{k,2};
                A_truth = GTList{k,3};
                if constants.CLOSED_LOOP
                    P_est = UAV_List{k}.INS.r_t__t_b_INS;
                    V_est = UAV_List{k}.INS.v_t__t_b_INS;
                    A_est = UAV_List{k}.INS.C_t__b_INS;
                else
                    P_est = UAV_List{k}.INS.r_t__t_b_out;
                    V_est = UAV_List{k}.INS.v_t__t_b_out;
                    A_est = UAV_List{k}.INS.C_t__b_out;
                end
                [~] = plot_PVA(constants, P_truth, V_truth, A_truth, P_est, V_est, A_est, "TANG", k);
            case "3DPlot"
                figure
                hold on
                grid on
                axis equal
                view(3)
                
                for l = 1:numIMUs
                    GTP = GTList{l,1};
                    plot3(GTP(1,1:end-1), GTP(2,1:end-1), GTP(3,1:end-1), 'r')
                    if constants.CLOSED_LOOP
                        P = UAV_List{l}.INS.r_t__t_b_INS;
                    else
                        P = UAV_List{l}.INS.r_t__t_b_out;
                    end
                    plot3(P(1,1:end-1), P(2,1:end-1), P(3,1:end-1), 'b.')
                end
                xlabel("East (m)")
                ylabel("North (m)")
                zlabel("Up (m)")
                legend(["Reference Trajectories", "Mechanized UAV Positions"])
                title("All UAVs position over time")
            case "3DVideo"
                mkdir(dirName)
                filepath = strjoin([dirName,"PositionsOverTime.avi"],'/');
                v = VideoWriter(filepath);
                open(v);
                az = 0; el = 0;
                frameDiv = floor(size(GTList{1,1}, 2) / 150);
                
                for m = 1:size(GTList{1,1}, 2)
                    
                    
                    if mod(m, frameDiv) == 0
                        f = figure;
                        set(f, 'Visible', 'off')
                        M = size(GTList{1,1}, 2);
                        percent = m/M * 100;
                        fprintf("3D Video Percent Complete: %s\n", num2str(percent));
                        for l = 1:numIMUs
                            GTP = GTList{l,1};
                            plot3(GTP(1,1:m), GTP(2,1:m), GTP(3,1:m), 'r')
                            hold on
                            grid on
                            axis equal
                            if constants.CLOSED_LOOP
                                P = UAV_List{l}.INS.r_t__t_b_INS;
                            else
                                P = UAV_List{l}.INS.r_t__t_b_out;
                            end
                            plot3(P(1,1:m), P(2,1:m), P(3,1:m), 'b')
                        end
                        xlabel("East (m)")
                        ylabel("North (m)")
                        zlabel("Up (m)")
                        if constants.CLOSED_LOOP
                            d = norm(UAV_List{2}.INS.r_t__t_b_INS(:,m) - UAV_List{3}.INS.r_t__t_b_INS(:,m))/2 + 2;
                            xlim([UAV_List{1}.INS.r_t__t_b_INS(1,m) - d UAV_List{1}.INS.r_t__t_b_INS(1,m) + d])
                            ylim([UAV_List{1}.INS.r_t__t_b_INS(2,m) - d UAV_List{1}.INS.r_t__t_b_INS(2,m) + d])
                            zlim([UAV_List{1}.INS.r_t__t_b_INS(3,m) - d UAV_List{1}.INS.r_t__t_b_INS(3,m) + d])
                        else
                            d = norm(UAV_List{2}.INS.r_t__t_b_out(:,m) - UAV_List{3}.INS.r_t__t_b_out(:,m))/2 + 2;
                            xlim([UAV_List{1}.INS.r_t__t_b_out(1,m) - d UAV_List{1}.INS.r_t__t_b_out(1,m) + d])
                            ylim([UAV_List{1}.INS.r_t__t_b_out(2,m) - d UAV_List{1}.INS.r_t__t_b_out(2,m) + d])
                            zlim([UAV_List{1}.INS.r_t__t_b_out(3,m) - d UAV_List{1}.INS.r_t__t_b_out(3,m) + d])
                        end
        
                        legend(["Reference Trajectories", "Mechanized UAV Positions"])
        
                        title("All UAVs position over time")
                        if az == 0 && el == 0
                            [az, el] = view();
                        else
                            view(az, el);
                        end
                        frame = getframe(gcf);
                        writeVideo(v,frame);
                    end
                    %close all
                end
                close(v)
                
            case "PCAVideo"
                figure
                hold on
                grid on
                az = 0; el = 0;
                filepath = strjoin([dirName,"PCAOverTime.avi"],'/');
                v = VideoWriter(filepath);
                open(v);
                frameDiv = floor(size(GTList{1,1}, 2) / 150);
                
                for m = 1:size(GTList{1,1}, 2)
                    
                    
                    if mod(m, frameDiv) == 0
                        f = figure;
                        set(f, 'Visible', 'off')
                        M = size(GTList{1,1}, 2);
                        percent = m/M * 100;
                        fprintf("PCA Percent Complete: %s\n", num2str(percent));
                        for l = 1:numIMUs
                            GTP = GTList{l,1};
                            plot3(GTP(1,m), GTP(2,m), GTP(3,m), 'o', 'Color', 'r')
                            hold on
                            plot3(GTP(1,1:m), GTP(2,1:m), GTP(3,1:m), '--', 'Color', 'r')

                            grid on
                            axis equal
                            if constants.CLOSED_LOOP
                                P = UAV_List{l}.INS.r_t__t_b_INS;
                            else
                                P = UAV_List{l}.INS.r_t__t_b_out;
                            end
                            plot3(P(1,m), P(2,m), P(3,m), 'o', 'Color', 'b')
                            plot3(P(1,1:m), P(2,1:m), P(3,1:m), '--', 'Color', 'b')
                        end
                        C_t__s = swarm.GC_PCA.C_t__s_array(:,:,m-1);
                        r_t__t_s = swarm.GC_PCA.r_t__t_s_array(:,m-1);
                        plotTemp = r_t__t_s;
                        plot3([0 C_t__s(1,1)]+plotTemp(1),[0 C_t__s(2,1)]+plotTemp(2),[0 C_t__s(3,1)]+plotTemp(3),'g','Linewidth',1);
                        plot3([0 C_t__s(1,2)]+plotTemp(1),[0 C_t__s(2,2)]+plotTemp(2),[0 C_t__s(3,2)]+plotTemp(3),'m','Linewidth',1);
                        plot3([0 C_t__s(1,3)]+plotTemp(1),[0 C_t__s(2,3)]+plotTemp(2),[0 C_t__s(3,3)]+plotTemp(3),'k','Linewidth',1);
           
                        xlim([plotTemp(1) - 7.5 plotTemp(1) + 7.5])
                        ylim([plotTemp(2) - 7.5 plotTemp(2) + 7.5])
                        zlim([plotTemp(3) - 7.5 plotTemp(3) + 7.5])
                        xlabel("East (m)")
                        ylabel("North (m)")
                        zlabel("Up (m)")
        
                        legend(["Reference Trajectories", "Reference Traj. Path", "Mechanized UAV Positions", "Mechanized Path"])
        
                        title("All UAVs position over time with PCA")
                        
                        if az == 0 && el == 0
                            [az, el] = view();
                        else
                            view(az, el);
                        end
                        frame = getframe(gcf);
                        writeVideo(v,frame);
                    end
                    %close all
                end
                close(v)
            case "IMUPlot"
                k = UAVSelect;
                f_truth_array = swarm.UAVList{k}.IMU.f_truth_array;
                f_est_array = swarm.UAVList{k}.IMU.f_est_array;
                w_truth_array = swarm.UAVList{k}.IMU.w_truth_array;
                w_est_array = swarm.UAVList{k}.IMU.w_est_array;
                N = size(f_truth_array, 2);
                figure
                subplot(2,2,1)
                plot(1:N, f_truth_array, 'r')
                hold on
                grid on
                scatter(1:N, f_est_array, 1, 'ok')
                hold off
                legend(["True Specific Force", "Measured"])
                
                xlabel("Samples")
                ylabel("Specific Force (f^b_{ib}) (m/s^2)")
                title("True Specific Force vs. Measured")
                subplot(2,2,2)
                plot(1:N, w_truth_array, 'b')
                hold on
                grid on
                scatter(1:N, w_est_array, 1, 'ok')
                hold off
                legend(["True Angular Velocity", "Measured"])
                
                xlabel("Samples")
                ylabel("Angular Velocity (\omega^b_{ib}) (rad/s)")
                title("True Angular Velocity vs. Measured")
                subplot(2,2,3)
                grid on
                scatter(1:N, f_truth_array - f_est_array, 1, 'or')
                hold off
                legend("Specific Force Error")
                
                xlabel("Samples")
                ylabel("Specific Force Error (f^b_{ib}) (m/s^2)")
                title("Error in the Specific Force Vector")
                subplot(2,2,4)
                grid on
                scatter(1:N, w_truth_array - w_est_array, 1, 'ob')
                hold off
                legend("Angular Velocity Error")
                
                xlabel("Samples")
                ylabel("Angular Velocity Error (\omega^b_{ib}) (rad/s)")
                title("Error in the Angular Velocity Error")
            case "RelError"
                
                figure
                subplot(2,2,1)
                hold on
                Legend = cell(numIMUs-1, 1);
                for k = 2:numIMUs
                    %%%% FIX LATER: index starting at 25 due to initial
                    %%%% jump 
                    if constants.CLOSED_LOOP
                        P = vecnorm(UAV_List{k}.INS.r_t__t_b_INS(:,25:end) - UAV_List{1}.INS.r_t__t_b_INS(:,25:end));
                    else
                        P = vecnorm(UAV_List{k}.INS.r_t__t_b_out(:,25:end) - UAV_List{1}.INS.r_t__t_b_out(:,25:end));
                    end
                    true_P = norm(P(:, 1));
                    N = size(P, 2);
                    %subplot(1,3,1)
                    scatter(1:N, P - true_P)
                    
                    Legend{k-1} = sprintf("UAV %s Pos. Error", num2str(k));
                end
                % yline(.02, '--', 'GPS Error is up at 3m ^^')
                xlabel("Samples")
                ylabel("Relative Position Error (m)")
                title("Relative Position Error for all UAVs From Ideal Formation")
                legend(Legend)
                subplot(2,2,2)
                hold on
                
                if constants.CLOSED_LOOP
                    P_temp = UAV_List{1}.INS.r_t__t_b_INS;
                else
                    P_temp = UAV_List{1}.INS.r_t__t_b_out;
                end
                N = size(P_temp, 2);
                sumError = zeros(3, N);
            
                for k = 2:numIMUs
                    if constants.CLOSED_LOOP
                        P = (UAV_List{1}.INS.GT.r_t__t_b{1} - UAV_List{k}.INS.GT.r_t__t_b{k}) - (UAV_List{1}.INS.r_t__t_b_INS - UAV_List{k}.INS.r_t__t_b_INS);
                    else
                        P = (UAV_List{1}.INS.GT.r_t__t_b{1} - UAV_List{k}.INS.GT.r_t__t_b{k}) - (UAV_List{1}.INS.r_t__t_b_out - UAV_List{k}.INS.r_t__t_b_out);
                    end
                    %for l = 2:numIMUs
                    sumError(:, :) = sumError(:, :) + P;% - norm(true_P);
                    %end
                end

                MSE_pos_x = 1/(numIMUs - 1) .* sumError(1,:).^2;
                MSE_pos_y = 1/(numIMUs - 1) .* sumError(2,:).^2;
                MSE_pos_z = 1/(numIMUs - 1) .* sumError(3,:).^2;
                RMSE_pos_x = sqrt(MSE_pos_x);
                RMSE_pos_y = sqrt(MSE_pos_y);
                RMSE_pos_z = sqrt(MSE_pos_z);
                % plot(1:N, MSE, 'g')
                % xlabel("Samples")
                % ylabel("Mean Squared Error of Distance (m)")
                % title("Mean Squared Error (MSE)")
                % grid on
                % legend("Distance Error")
                subplot(2,2,2)
                plot(1:N, RMSE_pos_x, 1:N, RMSE_pos_y, 1:N, RMSE_pos_z)

                %plot(1:N, RMSE, 'b')
                % hold on
                yline(30e-3, '--', 'Lower Bound of Ranger Accuracy')
                yline(50e-3, '--', 'Upper Bound of Ranger Accuracy')
                hold on
                % ylim([0 0.06])
                xlabel("Samples")
                ylabel("Root Mean Squared Error of Position (m)")
                title("Root Mean Squared Error (RMSE)")
                grid on
                legend("Position Error")
                
                swarm.PosError = sumError;
                swarm.MSE = MSE;
                swarm.RMSE = RMSE;
                
                %%% Orientation %%%
                subplot(2,2,3)
                hold on
                Legend = cell(numIMUs-1, 1);
                N = size(UAV_List{1}.INS.C_t__b_INS,3);
                psi_mag = zeros(1, N);
                psi_mag_all = zeros(numIMUs, N);
                for k = 2:numIMUs
                    if constants.CLOSED_LOOP
                        C_1 = UAV_List{1}.INS.GT.C_t__b{1};
                        C_n = UAV_List{k}.INS.C_t__b_INS;
                    else
                        C_1 = UAV_List{1}.INS.GT.C_t__b{1};
                        C_n = UAV_List{k}.INS.C_t__b_out;
                    end
                    C_truth = C_1(:,:,1).'*C_n(:,:,1);
                    for o = 1:size(C_1, 3)
                        C_1__n = C_1(:,:,o).'*C_n(:,:,o);
                        %deltaC = C_truth*C_1__n.';
                        deltaC = C_1(:,:,o) * C_n(:,:,o)';
                        psi_err_cross = deltaC-eye(3);
                        psi = [psi_err_cross(3,2); psi_err_cross(1,3); psi_err_cross(2,1)];
                        psi_mag(o) = norm(psi);
                        % q1 = math.dcm2q(C_1__n);
                        % q2 = math.dcm2q(C_truth);
                        % 
                        % qdelta = math.q1xq2(q1, [q2(1); -q2(2); -q2(3); -q2(4)]);
                        % theta = atan2(norm(qdelta(2:4)), qdelta(1));
                        % 
                        % psi_mag(o) = norm(theta);
                        % 
                        % if o == floor(size(C_1,3))-1
                        %     stophere = 1;
                        % end
                    end
                    scatter(1:N, psi_mag)
                    psi_mag_all(k, :) = psi_mag;
                    Legend{k-1} = sprintf("UAV %s Ori. Error", num2str(k));
                end
                xlabel("Samples")
                ylabel("Relative Orientation Error (^\circ)")
                title("Relative Orientation Error for all UAVs From Ideal Formation")
                legend(Legend)
                subplot(2,2,4)
                hold on
                sumError = sum(psi_mag_all);
                MSE = 1/(numIMUs-1) .* sumError.^2;
                RMSE = sqrt(MSE);
                swarm.OriError = sumError;
                % 
                % plot(1:N, MSE, 'g')
                % xlabel("Samples")
                % ylabel("Mean Squared Error of Orientation (^\circ)")
                % title("Mean Squared Error (MSE)")
                % grid on
                % legend("Ori. Error")
                % subplot(2,3,6)
                plot(1:N, RMSE, 'b')
                yline(constants.angularRes, '--', 'Angular Resolution of Sensor')
                xlabel("Samples")
                ylabel("Root Mean Squared Error of Orientation (^\circ)")
                title("Root Mean Squared Orientation Error (RMSE)")
                sgtitle("THESE RMSE VALUES NOT ACCURATE")
                grid on
                legend("Ori. Error")
            case "CovPlot"
                P_hist_psi_all = cell(1,numIMUs);
                P_hist_xyz_all = cell(1,numIMUs);

                for i = 1:numIMUs
                    P_hist_psi_all{i} = UAV_List{i}.INS.P_hist_psi;
                    P_hist_xyz_all{i} = UAV_List{i}.INS.P_hist_xyz;
                end
                
                for j = 1:numIMUs
                    for i = 1:N
                        P_hist_psi_trace(3*j-2:3*j,i) = diag(P_hist_psi_all{j}(:,:,i));
                        P_hist_xyz_trace(3*j-2:3*j,i) = diag(P_hist_xyz_all{j}(:,:,i));
                    end
                end
                
                temp_psi = P_hist_psi_trace(~isnan(P_hist_psi_trace));
                P_hist_xyz_trace = P_hist_xyz_trace(:,4000:end);
                temp_xyz = P_hist_xyz_trace(~isnan(P_hist_xyz_trace));

                one_sigma_psi = std(temp_psi, 0, "all");
                one_sigma_xyz = std(temp_xyz, 0, "all");
                mean_xyz = mean(temp_xyz);

                figure
                subplot(1,2,1)
                grid on
                hold on
                plot(1:N, P_hist_psi_trace)
                title("Attitude Covariance")
                xlabel("Samples")
                ylabel("Covariance")

                yline(one_sigma_psi, 'r--', sprintf("1-\\sigma Covariance: %.2g", one_sigma_psi))
                
                legend_psi = [];
                Axes = ["x", "y", "z"];
                for k = 1:numIMUs
                    for l = 1:3
                        legend_psi = [legend_psi, sprintf("UAV #%i, %s-axis", k, Axes(mod(l-1,3)+1))];
                    end
                end
                legend(legend_psi)

                subplot(1,2,2)
                grid on
                hold on
                plot(4000:N, P_hist_xyz_trace)
                title("Position Covariance")
                xlabel("Samples")
                ylabel("Covariance")

                yline(one_sigma_xyz + mean_xyz, 'r--', sprintf("1-\\sigma Covariance: %.2g", one_sigma_xyz));

                legend_xyz = [];
                Axes = ["x", "y", "z"];
                for k = 1:numIMUs
                    for l = 1:3
                        legend_xyz = [legend_xyz, sprintf("UAV #%i, %s-axis", k, Axes(mod(l-1,3)+1))];
                    end
                end
                legend(legend_xyz)

            otherwise
                error("Input an actually valid parameter ding-dong")
        end
    end
    if ~suppressOUT
        figList = findobj(allchild(0), 'flat', 'Type', 'figure');
        for n = 1:length(figList)
            figHandle = figList(n);
            try
                figName = figHandle.Children(3).Title.String;
            catch
                try
                    figName = figHandle.Children(2).Title.String;
                catch
                    try
                        figName = figHandle.Children(1).Title.String;
                    catch
                        figName = "IDontKnowWhyTheresNoAutoName";
                    end
                end
            end
                
            figName = regexprep(figName,'[^a-zA-Z0-9\s]','');
            figName = strrep(figName, ' ', '_');
            fullFile = fullfile(dirName, figName, '.png');
            fullFile = strrep(fullFile, '\.', '.');
            %try %#ok<TRYNC>
            saveas(figHandle, fullFile);
            %end
        end
    end
end