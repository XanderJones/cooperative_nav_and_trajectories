classdef GC_PCA < handle
    %   This has a lot of redundant features that need purging. The
    %   fundamental use is updating S_star which is the consensus matrix
    %   for the swarm. This should just be incorporated into the swarm
    %   object. Remember, no PCA is being performed anymore.

    properties
        C_t__s
        C_s__t
        r_t__t_s

        C_t__s_array
        r_t__t_s_array

        % deltaC
        % deltaR
        r_s1__s1s2
        C_s2__s1

        S_star

        S_star_minus

        numAgents
    end

    methods
        
        function obj = GC_PCA(numAgents, GT)
            obj.numAgents = numAgents;
            obj.C_s__t = eye(3);
            obj.C_t__s = eye(3);
            
            obj.S_star = zeros(3, obj.numAgents);
            for n = 1:obj.numAgents
                obj.S_star(:,n) = GT.r_t__t_b{n}(:,1);
            end
        end

        function obj = updatePCA(obj, C_s2__s1, r_s1__s1s2)
            
            % test = obj.r_e__e_s
            % test2 = r_s1__s1s2
            obj.C_s2__s1 = C_s2__s1;
            obj.r_s1__s1s2 = r_s1__s1s2;
            obj.r_t__t_s = obj.r_t__t_s + obj.C_t__s*r_s1__s1s2;
            obj.C_t__s = obj.C_t__s * C_s2__s1.';

            obj.C_t__s_array(:,:,end+1) = obj.C_t__s;
            obj.r_t__t_s_array(:, end+1) = obj.r_t__t_s;  
        end


        function obj = updateS(obj, S_star)
            obj.S_star_minus = obj.S_star;
            obj.S_star = S_star;
        end

    end
end