function [finalError] = plot_PVA(constants, P_truth, V_truth, A_truth, P_est, V_est, A_est, mech, UAVNum)
%
% FUNCTION DESCRIPTION:
%   Plots PVA truth. If PVA estimates/measurements provides will also plot
%   same and PVA errors
%
% INPUTS:
%   P_truth = True position (meters)
%   V_truth = True velocity (meters/sec)
%   A_truth = True attitude/orientation as a rotation matrix
%   P_est   = Estimated/measured position (meters)
%   V_est   = Estimated/measured velocity (meters/sec)
%   A_est   = Estimated/measured attitude/orientation as a rotation matrix
%   mech    = String label of the mechanization case ('ECI', 'ECEF', 'TANG', or 'NAV')
%
% OUTPUTS:

N = size(P_truth(:,1:end-1));
N = N(2);
constants.t_end = N*constants.dt;

delta = "\delta";
psi = "\psi";

TESTSIZE = 0;
P_truth = P_truth(:,1:end-1);
V_truth = V_truth(:,1:end-1);
A_truth = A_truth(:,:,1:end-1);  
P_est = P_est(:,1:end-1);
V_est = V_est(:,1:end-1);
A_est = A_est(:,:,1:end-1);

switch mech     % Define labels for the plots
   case 'ECI'
      P_label = 'r^i_{ib}';
      V_label = 'v^i_{ib}';
      A_label = 'C^i_b';
   case 'ECEF'
      P_label = 'r^e_{eb}';
      V_label = 'v^e_{eb}';
      A_label = 'C^e_b';
   case 'NAV'
      P_label = 'L_b, \lambda_b, h_b';
      V_label = 'v^n_{eb}';
      A_label = 'C^n_b';
   case 'TANG'
      P_label = 'r^t_{tb}';
      V_label = 'v^t_{tb}';
      A_label = 'C^t_b';
   otherwise
      error('Please choose ECI, ECEF, TANG, or NAV for the mechanization');
end

I3 = eye(3);        % 3X3 Indentiy matrix
t_sec = (constants.t_start:constants.dt:constants.t_end)';        % Simulation time vector
t_sec = t_sec(1:end,:);
%N = constants.N - 1; 
if size(P_truth, 2) < size(t_sec, 1)
    t_sec = t_sec(1:end-1,:);
    %N = constants.N - 2; 
end
           % Length of the simulation time vector

% Plot the position
if isempty(P_est)  % PVA Estimate/Measurement is NOT Provided
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, P_truth(1,:),'r')
    ylabel('r_x (m)')
    legend('Truth')
    title(sprintf("UAV: %i, %s Mechanization Position: %s", UAVNum, mech, P_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Position ', P_label]);
    subplot(3,1,2)
    plot(t_sec, P_truth(2,:),'g')
    ylabel('r_y (m)')
    subplot(3,1,3)
    plot(t_sec, P_truth(3,:),'b')
    ylabel('r_z (m)')
    xlabel('Time (sec)')
else    % PVA Estimate/Measurement is Provided
    
    if TESTSIZE
        size(t_sec)
        size(P_truth)
        size(V_truth)
        size(A_truth)
    end

    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, P_truth(1,:),'r', t_sec, P_est(1,:),':k')
    ylabel('r_x (m)')
    legend('Truth','Est')
    title(sprintf("UAV: %i, %s Mechanization Position: %s", UAVNum, mech, P_label));
    subplot(3,1,2)
    plot(t_sec, P_truth(2,:),'g', t_sec, P_est(2,:),':k')
    ylabel('r_y (m)')
    legend('Truth','Est')
    subplot(3,1,3)
    plot(t_sec, P_truth(3,:),'b',t_sec, P_est(3,:),':k')
    ylabel('r_z (m)')
    legend('Truth','Est')
    xlabel('Time (sec)')
    
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, P_truth(1,:) - P_est(1,:),'r')
    ylabel('\deltar_x (m)')
    title(sprintf("UAV: %i, %s Mechanization Position Error %s: %s", UAVNum, mech, delta, P_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Position Error \delta', P_label]);
    subplot(3,1,2)
    plot(t_sec, P_truth(2,:) - P_est(2,:),'g')
    ylabel('\deltar_y (m)')
    subplot(3,1,3)
    plot(t_sec, P_truth(3,:) - P_est(3,:),'b')
    ylabel('\deltar_z (m)')
    xlabel('Time (sec)')    
end

% Plot the velocity
if isempty(V_est)  % PVA Estimate/Measurement is NOT Provided
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, V_truth(1,:),'r')
    ylabel('v_x (m/s)')
    legend('Truth')
    title(sprintf("UAV: %i, %s Mechanization Velocity: %s", UAVNum, mech, V_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Velocity ', V_label]);
    subplot(3,1,2)
    plot(t_sec, V_truth(2,:),'g')
    ylabel('v_y (m/s)')
    subplot(3,1,3)
    plot(t_sec, V_truth(3,:),'b')
    ylabel('v_z (m/s)')
    xlabel('Time (sec)')
else    % PVA Estimate/Measurement is Provided
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, V_truth(1,:),'r', t_sec, V_est(1,:),':k')
    ylabel('v_x (m/s)')
    legend('Truth','Est')
    title(sprintf("UAV: %i, %s Mechanization Velocity: %s", UAVNum, mech, V_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Velocity ', V_label]);
    subplot(3,1,2)
    plot(t_sec, V_truth(2,:),'g', t_sec, V_est(2,:),':k')
    ylabel('v_y (m/s)')
    legend('Truth','Est')
    subplot(3,1,3)
    plot(t_sec, V_truth(3,:),'b',t_sec, V_est(3,:),':k')
    ylabel('v_z (m/s)')
    legend('Truth','Est')
    xlabel('Time (sec)')
    
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, V_truth(1,:) - V_est(1,:),'r')
    ylabel('\deltav_x (m/s)')
    title(sprintf("UAV: %i, %s Mechanization Velocity Error %s: %s", UAVNum, mech, delta, V_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Velocity Error \delta', V_label]);
    subplot(3,1,2)
    plot(t_sec, V_truth(2,:) - V_est(2,:),'g')
    ylabel('\deltav_y (m/s)')
    subplot(3,1,3)
    plot(t_sec, V_truth(3,:) - V_est(3,:),'b')
    ylabel('\deltav_z (m/s)')
    xlabel('Time (sec)')    
end

% Plot the attitude
if isempty(A_est)  % PVA Estimate/Measurement is NOT Provided
    k_vec = zeros(3,N);                     % Angle/axis representation of A_truth (i.e. C matrix)
    for i=1:N 
        k_vec(:,i)     = math.dcm2k(A_truth(:,:,i)); % Convert Rotation matrix to an Angle/axis k-vector
    end
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec, k_vec(1,:),'r')
    ylabel('k_x')
    legend('Truth')
    title(sprintf("UAV: %i, %s Mechanization Attitude: %s as an (Angle/Axis) k vector", UAVNum, mech, A_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Attitude ', A_label,' as an (Angle/Axis) k vector']);
    subplot(3,1,2)
    plot(t_sec, k_vec(2,:),'g')
    ylabel('k_y')
    subplot(3,1,3)
    plot(t_sec, k_vec(3,:),'b')
    ylabel('k_z')
    xlabel('Time (sec)')
else    % PVA Estimate/Measurement is Provided
    k_vec = zeros(3,N);                         % Angle/axis representation of A_truth (i.e. C matrix)
    for i=1:N 
        k_vec(:,i)     = math.dcm2k(A_truth(:,:,i)); % Convert Rotation matrix to an Angle/axis k-vector
    end
    k_vec_est = zeros(3,N);                     % Angle/axis representation of A_est (i.e. C matrix)
    for i=1:N 
        k_vec_est(:,i) = math.dcm2k(A_est(:,:,i));   % Convert Rotation matrix to an Angle/axis k-vector
    end
    
    
    %kylim
    figure; grid on;
    subplot(3,1,1)
    plot(t_sec(1:end), k_vec(1,:),'r', t_sec(1:end), k_vec_est(1,:),':k')
    ylabel('k_x')
    legend('Truth','Est')
    %ylim([min(k_vec(1,:)) * 1.5, max(k_vec(1,:)) * 1.5])
    title(sprintf("UAV: %i, %s Mechanization Attitude: %s as an (Angle/Axis) k vector", UAVNum, mech, A_label));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Attitude ', A_label,' as an (Angle/Axis) k vector']);
    subplot(3,1,2)
    plot(t_sec(1:end), k_vec(2,:),'g', t_sec(1:end), k_vec_est(2,:),':k')
    %ylim([min(k_vec(2,:)) * 1.5, max(k_vec(2,:)) * 1.5])
    ylabel('k_y')
    legend('Truth','Est')
    subplot(3,1,3)
    plot(t_sec(1:end), k_vec(3,:),'b',t_sec(1:end), k_vec_est(3,:),':k')
    %ylim([min(k_vec(3,:)) * 1.5, max(k_vec(3,:)) * 1.5])
    ylabel('k_z')
    legend('Truth','Est')
    xlabel('Time (sec)')
    
    psi_ERR = zeros(3,N);                       % Initialize the attitude error vector
    for i=1:N                                   %                  T
        dC = A_truth(:,:,i) * A_est(:,:,i)' ;   % Delta_C = C C_est

        Psi_ERR_cross = dC - I3;                % The approx skew-symmetric matrix version of Psi_ERR
        psi_ERR(:,i)  = [Psi_ERR_cross(3,2);    % Row 3 Col 2 = x-component of vector
                         Psi_ERR_cross(1,3);    % Row 1 Col 3 = y-component of vector
                         Psi_ERR_cross(2,1)];   % Row 2 Col 1 = z-component of vector
    end

    figure; grid on;
    subplot(3,1,1)
    plot(t_sec(1:end), psi_ERR(1,:)*180/pi,'r')
    ylabel('\psi_x (�)')
    title(sprintf("UAV: %i, %s Mechanization Attitude Error %s: %s as a %s vector", UAVNum, mech, delta, A_label, psi));

    %title(['UAV: ', UAVNum, ' , ', mech,' Mechanization Attitude Error \Delta', A_label,' as a \psi vector']);
    subplot(3,1,2)
    plot(t_sec(1:end), psi_ERR(2,:)*180/pi,'g')
    ylabel('\psi_y (�)')
    subplot(3,1,3)
    plot(t_sec(1:end), psi_ERR(3,:)*180/pi,'b')
    ylabel('\psi_z (�)')
    xlabel('Time (sec)')    
end

if ~isempty(A_est)
    finalError = [abs((P_est(1,end-1)-P_truth(1,end-1))/P_truth(1,end-1)*100), abs((P_est(2,end-1)-P_truth(2,end-1))/P_truth(2,end-1)*100), abs((P_est(3,end-1)-P_truth(3,end-1))/P_truth(3,end-1)*100)
                  abs((V_est(1,end-1)-V_truth(1,end-1))/V_truth(1,end-1)*100), abs((V_est(2,end-1)-V_truth(2,end-1))/V_truth(2,end-1)*100), abs((V_est(3,end-1)-V_truth(3,end-1))/V_truth(3,end-1)*100)
                  abs((A_est(1,end-1)-A_truth(1,end-1))/A_truth(1,end-1)*100), abs((A_est(2,end-1)-A_truth(2,end-1))/A_truth(2,end-1)*100), abs((A_est(3,end-1)-A_truth(3,end-1))/A_truth(3,end-1)*100)];
else
    finalError = [0, 0, 0; 0, 0, 0; 0, 0, 0];
end

end
