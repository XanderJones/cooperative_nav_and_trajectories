classdef IMU < handle
    % OBJECT DESCRIPTION:
    %   The IMU object handles measurements from perfect data and storing
    %   the Markov chain
    %
    % PROPERTIES:
    %   The specs, constants, ground truth properties dont
    %   change. The 4 IMU variables change with time and are unique to each
    %   object handle. currentIndex is the sample number from ground truth
    %
    %   The index object handles assigning which UAV holds the IMU. Do not
    %   change. (You actually cant, it is immutable)
    %
    % METHODS:
    %   getMeasurement returns the next measurement in sequence from ground
    %   truth data
    
    properties (SetAccess = private)
        IMUSpecs
        constants
        groundTruth
        currentIndex = 0
        
        b_g_BI = 0;
        b_a_BI = 0;
        wk_g = 0;
        wk_a = 0;

        f_truth_array
        f_est_array
        w_truth_array
        w_est_array
    end
    properties (SetAccess = immutable)
        index
    end
    methods

        function obj = IMU(index, groundTruth, constants)
            obj.constants = constants;
            obj.groundTruth = groundTruth;
            obj.index = index;
            
            N = size(obj.groundTruth.w_b__ib{1}, 2);
            obj.f_truth_array = zeros(3, N);
            obj.f_est_array = zeros(3, N);
            obj.w_truth_array = zeros(3, N);
            obj.w_est_array = zeros(3, N);
        end

        %function [f_b__i_b_tilde, w_b__i_b_tilde] = getMeasurement(obj, imu_const)
        function [f_b__i_b_tilde, w_b__i_b_tilde] = getMeasurement(obj, imu_const, IMUnum)
            % FUNCTION DESCRIPTION:
            %   Simulates an IMU
            %
            % INPUTS:
            %   w_b__i_b        = Ideal (error-free) Gyro measurements (rad/s)
            %   f_b__i_b        = Ideal (error-free) Accel measurements (m/s^2)
            %
            % OUTPUTS:
            %   w_b__i_b_tilde  = Gyro measurements (rad/s)
            %   f_b__i_b_tilde  = Accel measurements (m/s^2)
            obj.currentIndex = obj.currentIndex + 1;

            

            
            w_b__i_b = obj.groundTruth.w_b__ib{obj.index}(:, obj.currentIndex);
            f_b__i_b = obj.groundTruth.f_b__ib{obj.index}(:, obj.currentIndex);
            
            Fs = obj.constants.Fs;
            %dt = obj.constants.dt;
            %--------------------------------------------------------------------------
            % Generate the IMU measurements
            %--------------------------------------------------------------------------
            
            % Gyro Model:
            %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            I = eye(3);
            M_g = imu_const.gyro.M_g; 
            G_g = imu_const.gyro.G_g;
            if obj.constants.IMU_CALIBRATED
                G_g = zeros(3);
                M_g = zeros(3);
            end

            
            b_g_BI_sigma = imu_const.gyro.b_g_BI_sigma; 
            Tc_g = imu_const.gyro.BI.correlation_time;
            
            % Bias terms
            b_g_FB = imu_const.gyro.b_g_FB;
            b_g_BS = imu_const.gyro.b_g_BS;             % Bias - Bias Stability Bias term (m/s^2)
            
            %test = obj.b_g_BI;
            [obj.b_g_BI,obj.wk_g] = first_order_Markov(b_g_BI_sigma,Tc_g,Fs,obj.b_g_BI,obj.wk_g); % Bias - Markov Bias Instability Bias term (rad/s)

            b_g = obj.b_g_BI + b_g_FB + b_g_BS;             % All three bias terms (rad/s)
            
            % Noise terms
            %Tc_gyro = imu.gyro.BI.correlation_time;
            ARW = imu_const.gyro.ARW;  % Gyro Angle Random Walk (deg/rt-hr)
            PSD = (60*ARW)^(2);
            sigma_g = sqrt(Fs*PSD)*(pi/180)*(1/3600); %(rad/sec)
            
            w_g = sigma_g.*randn(3,1);        % Gyro noise (rad/sec)
            
            w_b__i_b_tilde = b_g + (I+M_g)*w_b__i_b + G_g*f_b__i_b + w_g;
            
            % Accelerometer Model
            %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            M_a = imu_const.accel.M_a;
            if obj.constants.IMU_CALIBRATED
                M_a = zeros(3);
            end
            Tc_a = imu_const.accel.BI.correlation_time;
            b_a_BI_sigma = imu_const.accel.b_a_BI_sigma;
            
            % Bias terms
            b_a_FB = imu_const.accel.b_a_FB;
            b_a_BS = imu_const.accel.b_a_BS;             % Bias - Bias Stability Bias term (m/s^2)
            
            
            [obj.b_a_BI,obj.wk_a] = first_order_Markov(b_a_BI_sigma,Tc_a,Fs,obj.b_a_BI,obj.wk_a); % Bias - Markov Bias Instability Bias term (rad/s)
            


            b_a = obj.b_a_BI + b_a_FB + b_a_BS;             % All three bias terms (m/s^2)
            
            % Noise terms
            VRW = imu_const.accel.VRW;          % Accel Angle Random Walk ((m/s^2)/rt-Hz)

            if obj.constants.MAKEWORSEIMU
                VRW = VRW * 10000;
            end
            
            %w_a = sqrt(Fs*(VRW)^2).* randn(3,1);        % Accelerometer noise (in m/s^2)
            w_a = sqrt(Fs*(VRW)^2) .* randn(3,1);

            f_b__i_b_tilde = b_a+(I+M_a)*f_b__i_b+w_a;    % lecture 2/14 Slide # 11

            %f_b__i_b_tilde - f_b__i_b

            if obj.constants.IDEALIMU
                f_b__i_b_tilde = f_b__i_b;
                w_b__i_b_tilde = w_b__i_b;
            end

            obj.f_truth_array(:,obj.currentIndex) = f_b__i_b;
            obj.f_est_array(:,obj.currentIndex) = f_b__i_b_tilde;
            obj.w_truth_array(:,obj.currentIndex) = w_b__i_b;
            obj.w_est_array(:,obj.currentIndex) = w_b__i_b_tilde;

            stophere = 1;



        end

    end
end