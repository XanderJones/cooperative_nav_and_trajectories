%--------------------------------------------------------------------------
%% IMU error characterization values - TEMPLATE
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Gyro specific terms

% Bias terms
imu_const.gyro.b_g_FB =  ;      % Bias - Fixed Bias term (rad/s)
imu_const.gyro.b_g_BS =  ;  % Bias - Bias Stability Bias term (rad/s)
imu_const.gyro.b_g_BI_sigma =  ;  % Bias - Bias Instability Bias term 1-sigma (rad/s)
imu_const.gyro.BI.correlation_time =  ; % Correlation time for the bias instability (sec)

% Noise terms
imu_const.gyro.ARW =  ;      % Gyro Angle Random Walk (deg/rt-hr)

% Scale factor stability & misalignment terms
s_g_x =  ;             % x-axis scale factor error (ppm * 1e-6)
s_g_y =  ;             % y-axis scale factor error (ppm * 1e-6);
s_g_z =  ;             % z-axis scale factor error (ppm * 1e-6);

m_g_xy =   ;               % Misalignment of y-axis into x-axis (in rad)
m_g_xz =   ;               % Misalignment of z-axis into x-axis
m_g_yx =   ;               % Misalignment of x-axis into y-axis
m_g_yz =   ;               % Misalignment of z-axis into y-axis
m_g_zx =   ;               % Misalignment of x-axis into z-axis
m_g_zy =   ;               % Misalignment of y-axis into z-axis

imu_const.gyro.M_g = ...
   [s_g_x , m_g_xy, m_g_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_g_yx, s_g_y , m_g_yz; ...
    m_g_zx, m_g_zy, s_g_z ];
   
g_sens =  ;  % Gyro G-sensitivity (rad/sec/g)

imu_const.gyro.G_g = ...        % The gyro G-sensitivity matrix (rad/sec/g)
   [g_sens , 0      , 0; ...    
    0      , g_sens , 0; ...
    0      , 0      , g_sens ];

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Accelerometer specific terms

% Bias terms
imu_const.accel.b_a_FB =  ;     % Bias - Fixed Bias term (m/s^2)
imu_const.accel.b_a_BS =  ;  % Bias - Bias Stability Bias term (m/s^2) 
imu_const.accel.b_a_BI_sigma =  ;  % Bias - Bias Instability Bias term 1-sigma (m/s^2) 
imu_const.accel.BI.correlation_time =  ; % Correlation time for the bias instability (sec)

% Noise terms
imu_const.accel.VRW =  ;     % Accel Angle Random Walk ((m/s^2)/rt-Hz)

% Scale factor stability & misalignment terms
s_a_x =  ;             % x-axis scale factor error (ppm * 1e-6)
s_a_y =  ;             % y-axis scale factor error (ppm * 1e-6);
s_a_z =  ;             % z-axis scale factor error (ppm * 1e-6);

m_a_xy =  ;               % Misalignment of y-axis into x-axis (in rad)
m_a_xz =  ;               % Misalignment of z-axis into x-axis
m_a_yx =  ;               % Misalignment of x-axis into y-axis
m_a_yz =  ;               % Misalignment of z-axis into y-axis
m_a_zx =  ;               % Misalignment of x-axis into z-axis
m_a_zy =  ;               % Misalignment of y-axis into z-axis

imu_const.accel.M_a = ...
   [s_a_x , m_a_xy, m_a_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_a_yx, s_a_y , m_a_yz; ...
    m_a_zx, m_a_zy, s_a_z ];
%==============================================================================