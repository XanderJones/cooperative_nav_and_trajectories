
%--------------------------------------------------------------------------
% IMU error characterization values - KVH 1750 IMU
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Gyro specific terms
IMUSpec.generateBS = 1;
% Bias terms
IMUSpec.gyro.b_g_FB = 0;      % Bias - Fixed Bias term (rad/s)
IMUSpec.gyro.b_g_BS = 0.05 * (pi/180) * (1/60) * (1/60);  % Bias - Bias Stability Bias term (rad/s)

IMUSpec.gyro.b_g_BI = 0.05 * (pi/180) * (1/60) * (1/60);  % Bias - Bias Instability Bias term 1-sigma (rad/s)
IMUSpec.gyro.BI.correlation_time = 3600; % Correlation time for the bias instability (1 hr in sec)

% Noise terms
IMUSpec.gyro.ARW = 0.012;      % Gyro Angle Random Walk (deg/rt-hr)

% Scale factor stability & misalignment terms
s_g_x = 50 * 1e-6;             % x-axis scale factor error (ppm * 1e-6)
s_g_y = 50 * 1e-6;             % y-axis scale factor error (ppm * 1e-6);
s_g_z = 50 * 1e-6;             % z-axis scale factor error (ppm * 1e-6);

m_g_xy =  1.0e-3;               % Misalignment of y-axis into x-axis (in rad)
m_g_xz =  0.5e-3;               % Misalignment of z-axis into x-axis
m_g_yx = -1.0e-3;               % Misalignment of x-axis into y-axis
m_g_yz =  0.2e-3;               % Misalignment of z-axis into y-axis
m_g_zx = -0.5e-3;               % Misalignment of x-axis into z-axis
m_g_zy =  0.7e-3;               % Misalignment of y-axis into z-axis

IMUSpec.gyro.M_g = ...
   [s_g_x , m_g_xy, m_g_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_g_yx, s_g_y , m_g_yz; ...
    m_g_zx, m_g_zy, s_g_z ];
   
g_sens = 0.5*pi/180 * 1/60 * 1/60;  % Gyro G-sensitivity (rad/sec/g)

IMUSpec.gyro.G_g = ...        % The gyro G-sensitivity matrix (rad/sec/g)
   [g_sens , 0      , 0; ...    
    0      , g_sens , 0; ...
    0      , 0      , g_sens ];

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Accelerometer specific terms

% Bias terms
IMUSpec.accel.b_a_FB = 0;     % Bias - Fixed Bias term (m/s^2)
IMUSpec.accel.b_a_BS = 0.05 * 10^(-3);  % Bias - Bias Stability Bias term (m/s^2) (in terms of g, which is relative for IMU positioning)
IMUSpec.accel.b_a_BI = 0.05 * 10^(-3);  % Bias - Bias Instability Bias term 1-sigma (m/s^2)
IMUSpec.accel.BI.correlation_time = 3600; % Correlation time for the bias instability (1 hr in seconds)

% Noise terms
IMUSpec.accel.VRW = 0.12 * 10^(-3);     % Accel Angle Random Walk ((m/s^2)/rt-Hz)

% Scale factor stability & misalignment terms
s_a_x = 100 * 1e-6;             % x-axis scale factor error (ppm * 1e-6)
s_a_y = 100 * 1e-6;             % y-axis scale factor error (ppm * 1e-6);
s_a_z = 100 * 1e-6;             % z-axis scale factor error (ppm * 1e-6);

m_a_xy = -1.0e-3;               % Misalignment of y-axis into x-axis (in rad)
m_a_xz =  0.1e-3;               % Misalignment of z-axis into x-axis
m_a_yx =  0.5e-3;               % Misalignment of x-axis into y-axis
m_a_yz =  1.0e-3;               % Misalignment of z-axis into y-axis
m_a_zx =  0.5e-3;               % Misalignment of x-axis into z-axis
m_a_zy = -0.5e-3;               % Misalignment of y-axis into z-axis

IMUSpec.accel.M_a = ...
   [s_a_x , m_a_xy, m_a_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_a_yx, s_a_y , m_a_yz; ...
    m_a_zx, m_a_zy, s_a_z ];
