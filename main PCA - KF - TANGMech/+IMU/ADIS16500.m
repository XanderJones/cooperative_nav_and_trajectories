%--------------------------------------------------------------------------
%% IMU error characterization values - TEMPLATE
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Gyro specific terms

% Bias terms
imu_const.gyro.b_g_FB = 0 ;      % Bias - Fixed Bias term (rad/s)
imu_const.gyro.b_g_BS = 0 ;  % Bias - Bias Stability Bias term (rad/s)
imu_const.gyro.b_g_BI_sigma = 6.833*(pi/180)/3600 ;  % Bias - Bias Instability Bias term 1-sigma (rad/s)
imu_const.gyro.BI.correlation_time = 3600 ; % Correlation time for the bias instability (sec) (assumed)

% Noise terms
imu_const.gyro.ARW = 0.29 ;      % Gyro Angle Random Walk (deg/rt-hr)

% Scale factor stability & misalignment terms (zero because we assume
% calibrated)
s_g_x =  0;             % x-axis scale factor error (ppm * 1e-6)
s_g_y =  0;             % y-axis scale factor error (ppm * 1e-6);
s_g_z =  0;             % z-axis scale factor error (ppm * 1e-6);

m_g_xy =  0 ;               % Misalignment of y-axis into x-axis (in rad)
m_g_xz =  0 ;               % Misalignment of z-axis into x-axis
m_g_yx =  0 ;               % Misalignment of x-axis into y-axis
m_g_yz =  0 ;               % Misalignment of z-axis into y-axis
m_g_zx =   0;               % Misalignment of x-axis into z-axis
m_g_zy =   0;               % Misalignment of y-axis into z-axis

imu_const.gyro.M_g = ...
   [s_g_x , m_g_xy, m_g_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_g_yx, s_g_y , m_g_yz; ...
    m_g_zx, m_g_zy, s_g_z ];
   
g_sens = 0 ;  % Gyro G-sensitivity (rad/sec/g) (assumed calibrated)

imu_const.gyro.G_g = ...        % The gyro G-sensitivity matrix (rad/sec/g)
   [g_sens , 0      , 0; ...    
    0      , g_sens , 0; ...
    0      , 0      , g_sens ];

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Accelerometer specific terms

% Bias terms
imu_const.accel.b_a_FB = 0 ;     % Bias - Fixed Bias term (m/s^2)
imu_const.accel.b_a_BS = 0 ;  % Bias - Bias Stability Bias term (m/s^2) 
imu_const.accel.b_a_BI_sigma = 0.000128 ;  % Bias - Bias Instability Bias term 1-sigma (m/s^2) 
imu_const.accel.BI.correlation_time = 3600 ; % Correlation time for the bias instability (sec)

% Noise terms
imu_const.accel.VRW =  0.001546;     % Accel Angle Random Walk ((m/s^2)/rt-Hz)

% Scale factor stability & misalignment terms
s_a_x =  0;             % x-axis scale factor error (ppm * 1e-6)
s_a_y =  0;             % y-axis scale factor error (ppm * 1e-6);
s_a_z =  0;             % z-axis scale factor error (ppm * 1e-6);

m_a_xy =  0;               % Misalignment of y-axis into x-axis (in rad)
m_a_xz =  0;               % Misalignment of z-axis into x-axis
m_a_yx =  0;               % Misalignment of x-axis into y-axis
m_a_yz =  0;               % Misalignment of z-axis into y-axis
m_a_zx =  0;               % Misalignment of x-axis into z-axis
m_a_zy =  0;               % Misalignment of y-axis into z-axis

imu_const.accel.M_a = ...
   [s_a_x , m_a_xy, m_a_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_a_yx, s_a_y , m_a_yz; ...
    m_a_zx, m_a_zy, s_a_z ];
%==============================================================================