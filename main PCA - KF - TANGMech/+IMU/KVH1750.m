%--------------------------------------------------------------------------
%% IMU error characterization values - KVH 1750 IMU
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Gyro specific terms

% Bias terms
imu_const.gyro.b_g_FB = 0;      % Bias - Fixed Bias term (rad/s)
imu_const.gyro.b_g_BS = 0;  % Bias - Bias Stability Bias term (rad/s)
imu_const.gyro.b_g_BI_sigma = 0.05*(pi/180)/3600;  % Bias - Bias Instability Bias term 1-sigma (rad/s)
imu_const.gyro.BI.correlation_time = 3600; % Correlation time for the bias instability (sec)

% Noise terms
imu_const.gyro.ARW = 0.012;      % Gyro Angle Random Walk (deg/rt-hr)
%imu_const.gyro.ARW = 0.12; %%%FIXME

% Scale factor stability & misalignment terms
% s_g_x = 50 * 1e-6;             % x-axis scale factor error (ppm * 1e-6)
% s_g_y = 50 * 1e-6;             % y-axis scale factor error (ppm * 1e-6);
% s_g_z = 50 * 1e-6;             % z-axis scale factor error (ppm * 1e-6);

s_g_x = 224 * 1e-7;            % x-axis scale factor error (ppm * 1e-6)
s_g_y = 740 * 1e-7;              % y-axis scale factor error (ppm * 1e-6);
s_g_z = 14 * 1e-7;             % z-axis scale factor error (ppm * 1e-6);

m_g_xy = -6.4e-5;               % Misalignment of y-axis into x-axis (in rad)
m_g_xz = -7.3e-5;               % Misalignment of z-axis into x-axis
m_g_yx =  6.6e-5;               % Misalignment of x-axis into y-axis
m_g_yz = -6.7e-5;               % Misalignment of z-axis into y-axis
m_g_zx =  3.8e-5;               % Misalignment of x-axis into z-axis
m_g_zy =  1.2e-5;               % Misalignment of y-axis into z-axis

imu_const.gyro.M_g = ...
   [s_g_x , m_g_xy, m_g_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_g_yx, s_g_y , m_g_yz; ...
    m_g_zx, m_g_zy, s_g_z ];


   
% g_sens = 2.4241*10^-6;  % Gyro G-sensitivity (rad/sec/g)
% g_sens = g_sens/9.81; % Gyro g-sensitivity in rad/s/(m/s^2)

g_sens = (0.5*(pi/180))/3600/9.81;

imu_const.gyro.G_g = ...        % The gyro G-sensitivity matrix (rad/sec/g)
   [g_sens , 0      , 0; ...    
    0      , g_sens , 0; ...
    0      , 0      , g_sens ];

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Accelerometer specific terms

% Bias terms
imu_const.accel.b_a_FB = 0;     % Bias - Fixed Bias term (m/s^2)
imu_const.accel.b_a_BS = 0;  % Bias - Bias Stability Bias term (m/s^2) 
imu_const.accel.b_a_BI_sigma = 0.05e-3;  % Bias - Bias Instability Bias term 1-sigma (m/s^2) 
imu_const.accel.BI.correlation_time = 3600; % Correlation time for the bias instability (sec)

% Noise terms
imu_const.accel.VRW = 0.12*10^-3*9.81;     % Accel Angle Random Walk ((m/s^2)/rt-Hz)

% Scale factor stability & misalignment terms
s_a_x = 437 * 1e-7;             % x-axis scale factor error (ppm * 1e-6)
s_a_y = 436 * 1e-7;             % y-axis scale factor error (ppm * 1e-6);
s_a_z = 523 * 1e-7;             % z-axis scale factor error (ppm * 1e-6);

m_a_xy = -6.2e-5;               % Misalignment of y-axis into x-axis (in rad)
m_a_xz = -7.4e-5;               % Misalignment of z-axis into x-axis
m_a_yx =  9.1e-5;               % Misalignment of x-axis into y-axis
m_a_yz =  4.1e-5;               % Misalignment of z-axis into y-axis
m_a_zx =  4.8e-5;               % Misalignment of x-axis into z-axis
m_a_zy = -4.5e-5;               % Misalignment of y-axis into z-axis

imu_const.accel.M_a = ...
   [s_a_x , m_a_xy, m_a_xz; ... % The combined Misalignment / Scale Factor matrix (dimensionless)
    m_a_yx, s_a_y , m_a_yz; ...
    m_a_zx, m_a_zy, s_a_z ];
%==============================================================================% 
