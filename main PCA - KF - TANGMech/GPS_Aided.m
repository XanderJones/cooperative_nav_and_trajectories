classdef GPS_Aided < INS_Interface
    %   UNAIDED_INS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        counter double = 1
    end
    properties (SetAccess = immutable)
        index
        constants
    end
    properties (SetAccess = public)
        GT
        GC_PCA
        
        IMU
        imu_const

        PCA_Update = 0

        r_t__t_b_INS
        v_t__t_b_INS%    <--- These must be implemented but are unused
        C_t__b_INS

        a_array

        kalman
        f_b__i_b

    end

    methods
        function obj = GPS_Aided(index, constants, GC_PCA, gt, imu_const, length)
            
            % Object Initialization

            obj.index = index;
            obj.constants = constants;
            obj.GC_PCA = GC_PCA;
            obj.imu_const = imu_const;

            K = length;

            obj.GT=gt;

            obj.r_t__t_b_INS = repmat(gt.r_t__t_b{index}(:,1),[1 K]);
            obj.v_t__t_b_INS = repmat(gt.v_t__t_b{index}(:,1),[1 K]);
            obj.C_t__b_INS = repmat(gt.C_t__b{index}(:,:,1),[1 1 K]);
            
            kalmanInitGPS;
            obj.kalman = kalman;
        end

        function obj = updatePCA(obj, PCA)
            obj.PCA_Update = PCA;
        end
        
        function obj = iterate(obj, w_b__i_b, f_b__i_b)
            obj.counter = obj.counter + 1;
            i = obj.counter;

            P = obj.r_t__t_b_INS(:,i-1);
            V = obj.v_t__t_b_INS(:,i-1);
            A = obj.C_t__b_INS(:,:,i-1);

            % w_b__i_b = obj.GT.w_b__ib{1}(:,i);
            % f_b__i_b = obj.GT.f_b__ib{1}(:,i);

            [r_t__t_b_plus, v_t__t_b_plus, C_t__b_plus, a_t__t_b] = TANG_mech(obj.constants,P,V,A,w_b__i_b,f_b__i_b); 

            % test1 = obj.GT.r_t__t_b{obj.index}(:,i-1);
            % test2 = test1 - P;
            % stophere = 1;

            obj.r_t__t_b_INS(:,i) = r_t__t_b_plus;% + obj.kalman.x(7:9, 1);
            obj.v_t__t_b_INS(:,i) = v_t__t_b_plus;% + obj.kalman.x(4:6, 1);
            obj.C_t__b_INS(:,:,i) = C_t__b_plus;
            obj.a_array(:,i) = a_t__t_b;
            obj.kalman.x(4:9,1) = [0;0;0;0;0;0];

            obj.f_b__i_b = f_b__i_b;

            %obj = obj.updateKalman();
            
        end

        function obj = updateKalman(obj)
            
            
            i = obj.counter;
            
            GPS_p = obj.GT.r_t__t_b{obj.index}(:, i) + obj.constants.sigma_r * randn(3, 1);
            GPS_v = obj.GT.v_t__t_b{obj.index}(:, i) + obj.constants.sigma_v * randn(3, 1);

            % The z measurement vector is the GPS velocity and position minues
            % the estimated velocity and position

            % obj.kalman.z = r_meas - obj.r_t__t_b_INS(:,i);
            obj.kalman.z = [GPS_v - obj.v_t__t_b_INS(:,i); GPS_p - obj.r_t__t_b_INS(:,i)];

            % This is more accurately the estimated force error
            VRW = obj.imu_const.accel.VRW;
            ESTf_b__i_b = obj.f_b__i_b - [VRW; VRW; VRW];

            % Derive the estimated latitude from the estimated position
            r_e__e_b_INS = obj.GT.r_e__et + obj.GT.C_e__t*obj.r_t__t_b_INS(:, i);
            [ESTL_b, ~, ~] = nav.xyz2llhTANG(obj.constants, obj.r_t__t_b_INS);
            % Use our functions to derive the F and G matrices from our
            % calculated values
            %C_e__b_INS = obj.GT.C_e__t*obj.C_t__b_INS(:,:,i);
            obj.kalman.F = nav.F_matrix(obj.constants, obj.imu_const, obj.C_t__b_INS(:,:,i), ESTf_b__i_b, ESTL_b, r_e__e_b_INS, obj.GT.C_e__t);
            obj.kalman.G = nav.G_matrix(obj.C_t__b_INS(:,:,i));

            % Discretize F and update P
            obj.kalman.Phi = eye(15) + obj.kalman.F * (1/obj.constants.F_measure);
            obj.kalman.disc_Q = nav.iterate_Q(obj.constants.dt, obj.kalman.Phi, obj.kalman.G, obj.kalman.Q_cont);
            obj.kalman.P = obj.kalman.disc_Q + obj.kalman.Phi * obj.kalman.P * transpose(obj.kalman.Phi);

            % Using our updated matrices, update our Kalman gain
            obj.kalman.K = obj.kalman.P * transpose(obj.kalman.H) / (obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R);
            if cond(obj.kalman.H * obj.kalman.P * transpose(obj.kalman.H) + obj.kalman.R) > 1 * 10^6
                obj.kalman.K = zeros(15, 6);
                disp("SINGULAR - discard run.")
            end
            
            % Use the Kalman gain to update x and P
            obj.kalman.x = obj.kalman.K * obj.kalman.z;
            obj.kalman.P = ( eye(15) - obj.kalman.K * obj.kalman.H) * obj.kalman.P * transpose( eye(15) - obj.kalman.K * obj.kalman.H) + obj.kalman.K * obj.kalman.R * transpose(obj.kalman.K);    


        end
    end
end

