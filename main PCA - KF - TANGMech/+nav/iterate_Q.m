function [Q_plus] = iterate_Q(dt, Phi, G, Q) %ALL at k-1. Not notated for simplicity
    
    % This can honestly be done in the main file, honestly would probably
    % be better to do it that way but I'm not changing it now so oh well
    % load_constants;

    Q_plus = 0.5.*(Phi*G*Q*transpose(G)*transpose(Phi) + G*Q*transpose(G)) * dt;
    
end