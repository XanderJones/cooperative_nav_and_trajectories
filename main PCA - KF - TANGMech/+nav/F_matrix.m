function [F] = F_matrix(constants, imu_const, ESTC_t__b, ESTf_b__i_b, ESTL_b, ESTr_e__e_b, C_e__t)
    % This is simply an extremely convoluted way of writing out a single
    % matrix. Keep that in mind while looking through this
    F__psi_g = eye(3);
    F__v_a = eye(3);
    F__a_a = [-1/imu_const.accel.BI.correlation_time, 0, 0
              0, -1/imu_const.accel.BI.correlation_time, 0
              0, 0, -1/imu_const.accel.BI.correlation_time];                                                                                                    
    F__g_g = [-1/imu_const.gyro.BI.correlation_time, 0, 0
              0, -1/imu_const.gyro.BI.correlation_time, 0
              0, 0, -1/imu_const.gyro.BI.correlation_time];
    
    % Could probably get away with using a standard g value but oh well.
    % Also should probably change the function to accept just position and
    % call the xyz2llh function here, TODO later
    g_0 = 9.7803253359 * (1+0.001931853*(sin(ESTL_b))^2) / sqrt(1-constants.e^2 * (sin(ESTL_b))^2);

    Re = constants.R0 / sqrt(1-constants.e^2 * (sin(ESTL_b))^2);

    Ohm_e__i_e = constants.Ohm_i__i_e;
    C_t__e = C_e__t.';
    Ohm_t__i_e = C_t__e * Ohm_e__i_e * C_e__t;
    
    % From the formula in Groves
    r_e__e_S = Re * sqrt(cos(ESTL_b)^2 + (1 - constants.e^2)*sin(ESTL_b)^2);

    r11 = -1 * Ohm_t__i_e; % same as e frame, cuz velocity, and no offset
    r12 = zeros(3);
    r13 = zeros(3);
    r14 = zeros(3);
    r15 = -1 * ESTC_t__b * F__psi_g;
    r21 = -1 * math.vec2ss(ESTC_t__b * ESTf_b__i_b);
    r22 = -2 * Ohm_t__i_e;
    r23 = (2 * g_0 / norm(r_e__e_S)) * (ESTr_e__e_b ./ (norm(ESTr_e__e_b)^2)) * transpose(ESTr_e__e_b);
    r24 = -1 * ESTC_t__b * F__v_a;
    r25 = zeros(3);
    r31 = zeros(3);
    r32 = eye(3);
    r33 = zeros(3);
    r34 = zeros(3);
    r35 = zeros(3);
    r41 = zeros(3);
    r42 = zeros(3);
    r43 = zeros(3);
    r44 = F__a_a;
    r45 = zeros(3);
    r51 = zeros(3);
    r52 = zeros(3);
    r53 = zeros(3);
    r54 = zeros(3);
    r55 = F__g_g;

    F = [r11, r12, r13, r14, r15
         r21, r22, r23, r24, r25
         r31, r32, r33, r34, r35
         r41, r42, r43, r44, r45
         r51, r52, r53, r54, r55];
end