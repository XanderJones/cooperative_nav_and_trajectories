function [G] = G_matrix(ESTC_t__b)
    % Overkill way of notating this matrix but I know it works so not
    % changing this either
    r11 = -1 * ESTC_t__b;
    r12 = zeros(3);
    r13 = zeros(3);
    r14 = zeros(3);
    r15 = zeros(3);
    r21 = zeros(3);
    r22 = -1 * ESTC_t__b;
    r23 = zeros(3);
    r24 = zeros(3);
    r25 = zeros(3);
    r31 = zeros(3);
    r32 = zeros(3);
    r33 = zeros(3);
    r34 = zeros(3);
    r35 = zeros(3);
    r41 = zeros(3);
    r42 = zeros(3);
    r43 = zeros(3);
    r44 = eye(3);
    r45 = zeros(3);
    r51 = zeros(3);
    r52 = zeros(3);
    r53 = zeros(3);
    r54 = zeros(3);
    r55 = eye(3);

    G = [r11, r12, r13, r14, r15
         r21, r22, r23, r24, r25
         r31, r32, r33, r34, r35
         r41, r42, r43, r44, r45
         r51, r52, r53, r54, r55];
end