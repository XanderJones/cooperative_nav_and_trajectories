function [P_out, V_out, A_out] = ECEF_mech(constants,P_in, V_in, A_in, w_b__i_b_tilde, f_b__i_b_tilde, fidelity)
% FUNCTION DESCRIPTION:
%   Implements the low-fidelity ECEF Mechanization
%
% INPUTS:
%   constants = structure containing constants
%   P_in    = Past position r_e__e_b(-) (meters)
%   V_in    = Past velocity r_e__e_b(-) (meters/sec)
%   A_in    = Past attitude/orientation as a rotation matrix C_e__b(-)
%   w_b__i_b_tilde = Current Gyro measurements
%   f_b__i_b_tilde = Current Accel measurements
%
% OUTPUTS:
%   P_out   = Updated position r_e__e_b(+) (meters)
%   V_out   = Updated velocity r_e__e_b(+) (meters/sec)
%   A_out   = Updated attitude/orientation as a rotation matrix C_e__b(+)

dt = constants.dt;      % Time step

% SYNTAX NOTE: "_p" indicates "prime," it's what I use to denote new vals

% ECEF Mechanization: One iteration of the mechanization

Omega_b__i_b = math.vec2ss(w_b__i_b_tilde);
Omega_e__i_e = constants.Ohm_i__i_e;
Omega_e__e_b = A_in * Omega_b__i_b * transpose(A_in) - Omega_e__i_e;

dTheta = norm(math.ss2vec(Omega_e__e_b).*dt);
k = math.ss2vec(Omega_e__e_b).*dt ./ dTheta;
kappa = math.vec2ss(k);

gamma_e__i_b = nav.gamma__i_b(constants, P_in);
g_e__b = gamma_e__i_b - Omega_e__i_e * Omega_e__i_e * P_in;

%--------------------------------------------------------------------------
% STEP 1.) Attitude Update
switch fidelity
    case "HIGH"
        C_e__b_p = (eye(3) + sin(dTheta)*kappa + (1-cos(dTheta))*kappa^2)*A_in;
    case "LOW"
        C_e__b_p = A_in*(eye(3) + Omega_b__i_b.*dt) - Omega_e__i_e*A_in.*dt;
    case "Q"
        q_in = dcm2q(A_in);
        q1 = [cos(dTheta/2); k.*sin(dTheta/2)];
        q_out = q1xq2(q1, q_in);
        C_e__b_p = q2dcm(q_out);
end
A_out = C_e__b_p;

%--------------------------------------------------------------------------
% STEP 2.) Specific Force Update
f_e__i_b_p = C_e__b_p * f_b__i_b_tilde;

%--------------------------------------------------------------------------
% STEP 3.) Velocity Update
v_e__e_b_p = V_in + (f_e__i_b_p + g_e__b - 2 .* Omega_e__i_e * V_in) .* dt;
V_out = v_e__e_b_p;

%--------------------------------------------------------------------------
% STEP 4.) Position Update
a_e__e_b = f_e__i_b_p + gamma_e__i_b;
r_e__e_b_p = P_in + V_in .* dt + a_e__e_b .* (dt^2 / 2);
P_out = r_e__e_b_p;

end
