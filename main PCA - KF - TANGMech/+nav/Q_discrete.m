function [Phi,Q] = Q_discrete(Q_cont,F,G,tau)

I_15x15 = eye(15);

%=============================
Phi = I_15x15 + F.*tau;
%=============================

Q = (1/2).*(Phi*G*Q_cont*transpose(G)*transpose(Phi) + G*Q_cont*transpose(G)).*tau;

end