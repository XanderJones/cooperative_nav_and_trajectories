function [P] = P_matrix(X)
    
    % This initializes the P_matrix from the values of x
    deltaPsi = X(1:3);
    deltaV = X(4:6);
    deltaR = X(7:9);
    deltaBg = X(10:12);
    deltaBa = X(13:15);

    r11 = diag(deltaPsi);
    r12 = zeros(3);
    r13 = zeros(3);
    r14 = zeros(3);
    r15 = zeros(3);
    r21 = zeros(3);
    r22 = diag(deltaV);
    r23 = zeros(3);
    r24 = zeros(3);
    r25 = zeros(3);
    r31 = zeros(3);
    r32 = zeros(3);
    r33 = diag(deltaR);
    r34 = zeros(3);
    r35 = zeros(3);
    r41 = zeros(3);
    r42 = zeros(3);
    r43 = zeros(3);
    r44 = diag(deltaBg);
    r45 = zeros(3);
    r51 = zeros(3);
    r52 = zeros(3);
    r53 = zeros(3);
    r54 = zeros(3);
    r55 = diag(deltaBa);

    P = [r11, r12, r13, r14, r15
         r21, r22, r23, r24, r25
         r31, r32, r33, r34, r35
         r41, r42, r43, r44, r45
         r51, r52, r53, r54, r55];
end