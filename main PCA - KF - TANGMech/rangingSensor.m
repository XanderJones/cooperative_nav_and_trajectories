function [rangeMeasurement, sigma_xyz] = rangingSensor(constants, rDiff, self_estimation, C_t__b_init, C_t__b1, seen)
    % FUNCTION DESCRIPTION:
    %   Produces a measurement given the current state of a UAV towards a
    %   second UAV object
    %
    % INPUTS:
    %   This should only be used by swarm, and the inputs are defined
    %   there, but reflect the current state of the swarm and true relative
    %   position and orientations.
    %   
    %
    % OUTPUTS:
    %   Outputs an estimate in the tangential frame of another UAV


    % Assumes sensor initially points directly forward

    % Values given for Hokuyo UTM-30LX-EW Scanning Laser Rangefinder (30m)
    D = norm(rDiff);
    %C_b1__b2 = C_t__b1.'*C_t__b2;
    [~,theta,psi] = math.dcm2rpy(C_t__b_init.'*C_t__b1);

    angularRes = constants.angularRes; %degrees
    % if D<10
    %     measAccuracy = 30*10^(-3); 
    % else 
    %     measAccuracy = 50*10^(-3); %meters
    % end

    measAccuracy = constants.neighbor_var;

    
    sigma_xyz = constants.neighbor_var;
    %sigma_xyz = sqrt(measAccuracy^2/3);

    
    maxVheadingOffset = 270/2; %degrees
    maxHheadingOffset = 270/2; %degrees
    maxRange = 30;%meters
    
    %headingErr = headingErr * pi/180;
    
    Vheading = theta; %degrees
    Hheading = psi; %degrees


    if D<maxRange && seen
        
        % phi = 2*pi*randn(1,1);
        % costheta = randn(1,1);
        % theta = acos(costheta);
        % 
        % angulardisturbance = angularRes*pi/180*randn(1);
        % 
        % x = sin(theta)*cos(phi);
        % y = sin(theta)*sin(phi);
        % z = cos(theta);
        % 
        % k = angulardisturbance * [x; y; z];

        % dcmError = math.aa2dcm(k);

        rnoise = angularRes * pi/180 * randn(1);
        pnoise = angularRes * pi/180 * randn(1);
        ynoise = angularRes * pi/180 * randn(1);

        dcmError = math.q2dcm(math.rpy2q([rnoise;pnoise;ynoise]));

        %C_b1__b2_tilde = dcmError*C_b1__b2;

        %measAccuracy = 0; %%% FIXME

        D = D + measAccuracy*randn();
        
        %r_t__b2b1 = dcmError*((rDiff/norm(rDiff)) * D); %%% FIXME

        r_t__b2b1 = ((rDiff/norm(rDiff)) * D);

        r_t__b1b2 = -1*r_t__b2b1;

        %rangeMeasurement = self_estimation + dcmError*r_t__b1b2;
        rangeMeasurement = self_estimation - r_t__b1b2; %%% 
        %rangeMeasurement = self_estimation - r_t__b1b2;
        %rangeMeasurement = self_estimation - rDiff;
        if sum(isnan(rangeMeasurement)) ~= 0
            stophere = 1;
        end

    else
        rangeMeasurement = "Damn, it's out of range :(";

    end



end