% FUNCTION DESCRIPTION:
%   A collection of mc_func calls to generate data

clear
primary_mont = 5;
do3d = 0;

% function mc_func(folderPath, mont, IMUnum, F_meas, trajectoryFile, ATT_AIDING, angRes, centerGPS, DEAD_RECKONING, ThreeD, GPSOnly)

trajFiles = ["./trajFiles/gt_uav_f8.mat", "./trajFiles/Scurve.mat", "./trajFiles/gt_uav_7stationary.mat", "./trajFiles/IMUS_2.mat", "./trajFiles/IMUS_5.mat", "./trajFiles/IMUS_9.mat", "./trajFiles/IMUS_21.mat"];
IMUnames = ["KVH1750", "IMU.Orientus", "STIM300", "ADIS16500"];
GPSnames = ["NoGPSCenter", "GPSCenter"];
allGPSnames = ["NoAllGPS", "AllGPS"];
trajnames = ["Fig8", "SCurve", "Stationary", "2IMUs3Lobe", "5IMUs3Lobe", "9IMUs3Lobe", "21IMUs3Lobe"];
attnames = ["NoAttitudeAiding", "AttitudeAiding"];

noREL = 0;
UKF = 0;
CLOSED_LOOP = 1;
counter = 1;

DEAD = 1;

for IMUtype = 1
    for trajFile = 2
        for allGPS = 0
            for attAid = 1
                for GPSCenter = 0

                    name =  strjoin([IMUnames(IMUtype), "_", GPSnames(GPSCenter+1), "_", allGPSnames(allGPS+1), "_", trajnames(trajFile), "_", attnames(attAid+1)], "")
                    mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, IMUtype, 100, trajFiles(trajFile), attAid, 0.25, GPSCenter, DEAD, 0, allGPS, noREL, UKF, CLOSED_LOOP)
                    namearray(counter) = name;
                    counter = counter + 1;

                end
            end
        end
    end
end
% for IMUtype = 1
%     for trajFile = 4:7
%         for allGPS = 0
%             for attAid = 1
%                 name = strjoin([IMUnames(IMUtype), "_", GPSnames(GPSCenter+1), "_", allGPSnames(allGPS+1), "_", trajnames(trajFile), "_", attnames(attAid+1)], "")
%                 %mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, IMUtype, 100, trajFiles(trajFile), attAid, 0.25, 0, 0, 0, allGPS, noREL, UKF, CLOSED_LOOP)
%                 namearray(counter) = name;
%                 counter = counter + 1;
%             end
%         end
%     end
% end
% GPSCenter = 1;
% allGPS = 0;
% attAid = 1;
% 
% for trajFile = 2
%     for IMUtype = 1:3
%         name = strjoin([IMUnames(IMUtype), "_", GPSnames(GPSCenter+1), "_", allGPSnames(allGPS+1), "_", trajnames(trajFile), "_", attnames(attAid+1)], "")
%         %mc_func(strjoin(["./mc_func_out/",name,"/"],""), primary_mont, IMUtype, 100, trajFiles(trajFile), attAid, 0.25, GPSCenter, 0, 0, allGPS, noREL, UKF, CLOSED_LOOP)
%         namearray(counter) = name;
%         counter = counter + 1;
%     end
% end
namearray